#ifndef RESOURCE_HPP
#define RESOURCE_HPP

#include <cassert>
#include <sys/resource.h>

class TimeUsage
{
public:
    enum class Unit
    {
        MIN,
        SEC,
        MSEC,
        USEC
    };

private:
    double user;   // in microseconds
    double system; // in microseconds

    template <Unit unit>
    static double from_microseconds(double value)
    {
        switch (unit)
        {
        case Unit::MIN:
            return value / 60000000;
        case Unit::SEC:
            return value / 1000000;
        case Unit::MSEC:
            return value / 1000;
        default:
            return value;
        }
    }

    TimeUsage(double user, double system)
        : user(user),
          system(system)
    {
    }

public:
    TimeUsage()
        : user(0.0),
          system(0.0)
    {
    }

    static TimeUsage now()
    {
        auto to_double = [](timeval t)
        {
            double res = t.tv_sec * 1000000 + t.tv_usec;
            return res;
        };

        struct rusage usage;
        getrusage(RUSAGE_SELF, &usage);

        return TimeUsage(to_double(usage.ru_utime), to_double(usage.ru_stime));
    }

    TimeUsage operator-(const TimeUsage& lhs)
    {
        return {user - lhs.user, system - lhs.system};
    }

    TimeUsage operator-=(const TimeUsage& lhs)
    {
        user -= lhs.user;
        system -= lhs.system;
        return *this;
    }

    TimeUsage operator+(const TimeUsage& lhs)
    {
        return {user + lhs.user, system + lhs.system};
    }

    TimeUsage operator+=(const TimeUsage& lhs)
    {
        user += lhs.user;
        system += lhs.system;
        return *this;
    }

    template <typename InputIt>
    static TimeUsage average(InputIt from, InputIt to)
    {
        TimeUsage res;
        std::size_t n;

        for (auto i = from; i != to; ++i)
        {
            res += *i;
            n++;
        }

        res.user /= n;
        res.system /= n;

        return res;
    }

    template <Unit unit = Unit::SEC>
    double user_time() const
    {
        return from_microseconds<unit>(user);
    }

    template <Unit unit>
    double system_time() const
    {
        return from_microseconds<unit>(system);
    }

    template <Unit unit = Unit::SEC>
    double total_time() const
    {
        return from_microseconds<unit>(user + system);
    }
};

class TimeUsageTracker
{
    TimeUsage at_start;
    TimeUsage accum;
    bool paused = false;

    TimeUsageTracker(TimeUsage at_start, TimeUsage accum, bool paused)
        : at_start(at_start),
          accum(accum),
          paused(paused)
    {
    }

public:
    static TimeUsageTracker create_started()
    {
        return TimeUsageTracker(TimeUsage::now(), {}, false);
    }

    static TimeUsageTracker create_paused() { return TimeUsageTracker({}, {}, true); }

    void reset()
    {
        at_start = TimeUsage::now();
        accum = {};
    }

    void pause()
    {
        assert(!paused);
        accum += TimeUsage::now() - at_start;
        paused = true;
    }

    void unpause()
    {
        assert(paused);
        at_start = TimeUsage::now();
        paused = false;
    }

    TimeUsage get_usage()
    {
        return (paused) ? accum : accum + TimeUsage::now() - at_start;
    }
};

// this is all we can check using sys/resource; to track memory more
// precisely, we would need to use a custom allocator, which would highly
// complicate our codebase
struct SpaceUsage
{
    // in kilobytes
    static long maximum()
    {
        struct rusage usage;
        getrusage(RUSAGE_SELF, &usage);

        return usage.ru_maxrss;
    }
};

#endif
