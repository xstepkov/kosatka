#pragma once

#include <cassert>
#include <cstring>

// Right now this is only efficient for primitive types, because I am lazy
template <typename T>
class SmallStack
{
    unsigned _size = 0;
    unsigned _capacity = 8;
    T* _data = new T[_capacity];

public:
    SmallStack()
        : _data(new T[_capacity])
    {
    }

    ~SmallStack() { delete[] _data; }

    SmallStack(SmallStack<T>&& other)
    {
        delete[] _data;

        _size = other._size;
        _capacity = other._capacity;
        _data = other._data;
    }

    SmallStack(const SmallStack<T>& other)
    {
        delete[] _data;
        _size = other._size;
        _capacity = other._capacity;

        _data = new T[_capacity];

        for (unsigned i = 0; i < _capacity; i++)
        {
            _data[i] = other._data[i];
        }
    }

    SmallStack& operator=(SmallStack<T>&& other)
    {
        delete[] _data;

        _size = other._size;
        _capacity = other._capacity;
        _data = other._data;

        return *this;
    }

    SmallStack& operator=(const SmallStack<T>& other)
    {
        delete[] _data;
        _size = other._size;
        _capacity = other._capacity;

        _data = new T[_capacity];

        for (unsigned i = 0; i < _capacity; i++)
        {
            _data[i] = other._data[i];
        }

        return *this;
    }

    unsigned size() const { return _size; }

    void push_back(T value)
    {
        if (_size == _capacity)
        {
            _capacity = _capacity * 2;
            T* tmp = new T[_capacity];

            std::memcpy(tmp, _data, sizeof(T) * _size);

            delete[] _data;
            _data = tmp;
        }

        _data[_size++] = value;
    }

    T& operator[](unsigned idx)
    {
        //assert(idx < _size);
        return _data[idx];
    }

    void shrink(unsigned new_size) { _size = new_size; }
};
