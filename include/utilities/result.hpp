#ifndef RESULT_HPP
#define RESULT_HPP

#include <functional>
#include <string>
#include <variant>

/*
 * This class implements functionality similar to the Rust Result
 * type. Results holds either a value of some type T or an error
 * message. This class can be used to avoid throwing an exception.
 */

template <typename T>
class Result
{
    // we use this wrapper to allow results which have strings as their
    // values
    struct ErrorMessage
    {
        std::string message;
        ErrorMessage(std::string message)
            : message(message)
        {
        }
    };

    const std::variant<T, ErrorMessage> content;

    Result(T result)
        : content(result)
    {
    }
    Result(ErrorMessage message)
        : content(message)
    {
    }

public:
    static Result<T> Ok(T value) { return Result(std::move(value)); }

    static Result<T> Error(std::string message)
    {
        return Result(ErrorMessage(std::move(message)));
    }

    inline bool is_ok() const { return std::holds_alternative<T>(content); }

    inline bool is_error() const { return std::holds_alternative<ErrorMessage>(content); }

    inline T get_value() const
    {
        // this function throws if content contains an error
        return std::get<T>(content);
    }

    inline std::string get_error_message() const
    {
        // this function throws if content contains a value
        return std::get<ErrorMessage>(content).message;
    }

    template <typename OtherType>
    inline Result<OtherType> forward_error() const
    {
        return Result<OtherType>::Error(get_error_message());
    }

    // feel free to ignore the following two functions, I wrote
    // them just for fun and I will (probably) not use them

    // functor
    template <typename OtherType>
    inline Result<OtherType> fmap(std::function<OtherType(const T&)> f) const
    {
        return (is_error()) ? Result<OtherType>::Error(get_error_message())
                            : Result<OtherType>::Ok(f(get_value()));
    }

    // monad
    template <typename OtherType>
    inline Result<OtherType> bind(std::function<Result<OtherType>(const T&)> f) const
    {
        return (is_error()) ? Result<OtherType>::Error(get_error_message())
                            : f(get_value());
    }
};

using EmptyResult = Result<std::monostate>;

#endif
