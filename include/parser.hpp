#pragma once

#include <optional>
#include <string>

#include "formula.hpp"

std::optional<Formula> parse_formula(const std::string& path);
