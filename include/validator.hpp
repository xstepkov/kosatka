#pragma once

#include <formula.hpp>

class Validator
{
    const Formula& formula;

public:
    Validator(const Formula& formula);
    bool validate(const std::vector<Literal>& model) const;
};
