#pragma once

#include <memory>

enum class RestartManagerType : char
{
    NO,
    CONSTANT,
    LUBY
};

class RestartManager
{
protected:
    // stores the number of conflicts since the last restart
    int _conflicts_since_last = 0;

public:
    static std::unique_ptr<RestartManager> choose(RestartManagerType type,
                                                  int restart_constant);
    virtual bool should_restart() = 0;

    virtual void register_conflict() { ++_conflicts_since_last; }

    virtual ~RestartManager() = default;
};

class NoRestartManager : public RestartManager
{
public:
    bool should_restart() override;
};

// always restarts after a fixed number of conflicts
class ConstantRestartManager : public RestartManager
{
    const int _restart_after;

public:
    ConstantRestartManager(int restart_after)
        : _restart_after{restart_after}
    {
    }

    bool should_restart() override;
};

class LubySequenceGenerator
{
    int _sum_of_last = 0;
    int _current = 1;
    int _next_reach = 2;

public:
    int next();
};

// restarts according to the Luby sequence multiplied by a constant
class LubyRestartManager : public RestartManager
{
    LubySequenceGenerator _generator;
    // restarts after the next number in the Luby sequence multiplied by _multiply_by
    const int _multiply_by;
    int _restart_after;

    int next() { return _generator.next() * _multiply_by; }

public:
    LubyRestartManager(int restart_factor)
        : _generator(),
          _multiply_by(restart_factor),
          _restart_after(next())
    {
    }

    bool should_restart() override;
};
