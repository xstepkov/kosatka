#pragma once

#include <climits>
#include <vector>

#include "formula.hpp"
#include "threeval.hpp"
#include "utilities/smallstack.hpp"

/*
 * This class keeps track of the current state of the solver, including
 * the state of the formula and the current state of the assignment.
 * This class is responsible for unit propagation, but it does not
 * choose variables or their phases.
 *
 * This is a single, heavily-optimized class. While it would be cleaner
 * to split it into several distinct classes, this approach can guarantee
 * greater performance.
 */
class Engine
{
public:
    enum class Status
    {
        SOLVED,
        CONFLICT,
        UNKNOWN
    };

    using TrailIdx = int;
    using DecisionLevel = int;
    using ClauseIdx = int;

    static const ClauseIdx NO_REASON = -1;
    static const ClauseIdx UNIT_LITERAL = -2;

private:
    static const DecisionLevel NO_LEVEL = -1;

    struct VariableInfo
    {
        // Smallstack is 8 bytes smaller than a vector, and since there
        // may be many vectors, it may be worth it
        SmallStack<ClauseIdx> watched_negated = {};
        SmallStack<ClauseIdx> watched_nonnegated = {};

        ClauseIdx reason = NO_REASON;
        DecisionLevel decision_level = NO_LEVEL;

        AssignmentState last_assignment_state = AssignmentState::UNKNOWN;
    };

    // This struct exists because, originally, there were other attributes
    struct ClauseInfo
    {
        Clause clause;

        ClauseInfo(Clause clause);
    };

    std::vector<VariableInfo> _variable_infos;

    // in a separate vector for better cache-friendliness
    std::vector<AssignmentState> _variable_states;

    std::vector<ClauseInfo> _clause_infos = {};

    std::vector<Literal> _trail = {};
    std::vector<TrailIdx> _decisions = {};
    DecisionLevel _highest_backtrack_level = NO_LEVEL;
    TrailIdx _earliest_flipped_decision = INT_MAX;

    // Invariant: after a successful unit propagation, this is equal to _trail.size()
    // Invariant: this is always less or equal to _trail.size()
    TrailIdx _next_unit_propagation = 0;

    std::vector<Variable> _unassigned_in_last_backtrack = {};

    int _original_clause_count;
    bool _trivial_unsat = false;

public:
    Engine(Formula formula);

    // QUERIES:

    const std::vector<Literal>& trail() const;

    int variable_count() const;
    int clause_count() const;
    int original_clause_count() const;

    const Clause& get_clause(ClauseIdx idx) const;

    // This method is valid when _next_unit_propagation == _trail.size()
    // In other cases, it may return an incorrect value
    AssignmentState clause_status(ClauseIdx idx) const;

    // This method is always valid (and works for untracked clauses), but
    // is slower than the previous one
    AssignmentState clause_status(const Clause& clause) const;

    AssignmentState variable_status(Variable variable) const;
    AssignmentState literal_status(Literal literal) const;

    Status status() const;

    // there are two contradicting unit clauses in the formula
    bool is_trivially_unsat() const;

    ClauseIdx reason(Variable variable) const;

    std::optional<DecisionLevel>
    lowest_unit_propagation_level(const Clause& clause) const;

    bool can_backtrack() const;
    DecisionLevel current_decision_level() const;
    std::optional<DecisionLevel> decision_level(Variable variable) const;
    std::optional<Variable> last_decision() const;
    bool at_current_decision_level(Variable variable) const;

    const std::vector<Variable>& unassigned_in_last_backtrack() const;

    AssignmentState last_assignment_state(Variable variable) const;

    // MODIFIERS:

    // Precondition: The clause does not violate the current assignment
    //
    // Returns true if the clause was added into the clause list,
    // false if it was not (because it for example only had a single
    // literal, and was directly unit propagated)
    bool learn(Clause clause);

    // Invariant: between calls to forget, indices of clauses do not change
    // Invariant: the indices of the clauses that the object was constructed
    // with does not change
    //
    // Note: This method can ignore some suggestions (notably, clauses with
    // two literals are never forgotten)
    //
    // Returns relocations of the clauses, that is, for each clause index
    // its new clause index after relocation; -1 means the clause was removed
    const std::vector<ClauseIdx>& forget(const std::vector<ClauseIdx>& indices);

    void reset();
    void backtrack_to(DecisionLevel level);
    void flip_last_decision();

    void decide(Variable variable, VariablePhase phase);
    std::optional<ClauseIdx> unit_propagate();

private:
    void assign(Variable variable, VariablePhase phase);
    void set_literal_to_true(Literal lit);

    void unassign_last();
    void clear_trail(TrailIdx from);

    void set_up_watch(ClauseIdx clause_idx, bool first);
};
