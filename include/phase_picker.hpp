#pragma once

#include "engine.hpp"
#include "formula.hpp"
#include <memory>

enum class PhasePickerType : char
{
    BASIC, SAVING
};

class PhasePicker
{
public:
    static std::unique_ptr<PhasePicker> choose(PhasePickerType type);
    virtual VariablePhase pick(Variable var, const Engine& engine) = 0;
    virtual ~PhasePicker() = default;
};

class BasicPhasePicker : public PhasePicker
{
public:
    VariablePhase pick(Variable var, const Engine& engine) override;
};

class SavingPhasePicker : public PhasePicker
{
    static constexpr VariablePhase _default = VariablePhase::NEGATIVE;

public:
    VariablePhase pick(Variable var, const Engine& engine) override;
};
