#pragma once

#include "formula.hpp"
#include <optional>

class AssignmentState
{
    enum class State : unsigned char
    {
        FALSE,
        TRUE,
        UNKNOWN
    };

    static State from_bool(bool b);
    static State from_phase(VariablePhase phase);

    State state;

    constexpr AssignmentState(State state)
        : state(state)
    {
    }

public:
    static const AssignmentState FALSE;
    static const AssignmentState TRUE;
    static const AssignmentState UNKNOWN;

    // the default value is an unknown assignment state
    AssignmentState();

    bool is_decided() const;
    bool is_unknown() const;
    bool is_true() const;
    bool is_false() const;

    std::optional<VariablePhase> to_phase() const;

    AssignmentState flipped() const;
    void flip();

    void assign(bool value);
    void assign(VariablePhase phase);
    void unassign();
};
