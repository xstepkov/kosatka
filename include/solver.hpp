#pragma once

#include "engine.hpp"
#include "forgetting_manager.hpp"
#include "formula.hpp"
#include "phase_picker.hpp"
#include "restart_manager.hpp"
#include "variable_picker.hpp"

#include <memory>

enum class SolverResult
{
    SAT,
    UNSAT,
    UNKNOWN,
};

/*
 * The solver interacts with the concrete and optimized class engine.
 * Additionally, it has other components which can be changed
 * during setup, such as the heuristic for picking variables and their
 * phases, the restart scheme, the forgetting strategy.
 */
class Solver
{
public:
    enum class SolverAlgorithm : char
    {
        DPLL,
        CDCL
    };

    // beware: some combinations are illegal, it is checked in the main
    // file, but not here
    struct Options
    {
        SolverAlgorithm algorithm;

        bool minimize;

        VariablePickerType variable_picker_type;

        PhasePickerType phase_picker_type;

        RestartManagerType restart_manager_type;
        int restart_constant;

        ForgettingManagerType forgetting_manager_type = ForgettingManagerType::VSIDS;
        int forgetting_constant = 1000;
        double forget_percentage = 0.6;

        bool minimize_model = false;
    };

private:
    Engine _engine;

    SolverAlgorithm _algorithm;
    bool _minimize = true;
    bool _minimize_model = false;

    std::unique_ptr<VariablePicker> _variable_picker;
    std::unique_ptr<PhasePicker> _phase_picker;
    std::unique_ptr<RestartManager> _restart_manager;
    std::unique_ptr<ForgettingManager> _forgetting_manager;

    Clause learn_from_conflict(Engine::ClauseIdx conflict_clause_idx) const;

    SolverResult run_DPLL();
    SolverResult run_CDCL();

public:
    Solver(Formula formula, Options options);

    SolverResult run();
    std::vector<Literal> get_model() const;

    friend class TestSolver;
};
