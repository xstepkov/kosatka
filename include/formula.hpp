#pragma once

#include <algorithm>
#include <cassert>
#include <cstdlib>
#include <functional>
#include <iostream>
#include <ostream>
#include <vector>

// Variable must be positive
using Variable = int;
struct Clause;

enum class VariablePhase : char
{
    POSITIVE,
    NEGATIVE
};

class Literal
{
    int value;

    // since both variables and literals are integers, we do not
    // want to expose this constructor, as that could be error-prone
    Literal(int value)
        : value(value)
    {
        // this gives us a guarantee that a literal is never zero,
        // and we do not need to check that using asserts
        // if (value == 0)
        // {
        //     throw std::logic_error("A literal can never be 0!");
        // }
    }

    // used only by the clause class
    Literal()
        : value(0)
    {
    }

    // hopefully, all of the methods will get inlined and there will be no
    // performance penalty due to this encapsulation
public:
    static Literal from_variable(Variable var, VariablePhase phase)
    {
        return (phase == VariablePhase::POSITIVE) ? non_negated(var) : negated(var);
    }

    static Literal negated(Variable var) { return Literal(-var); }

    static Literal non_negated(Variable var) { return Literal(var); }

    static Literal from_int(int value)
    {
        assert(value != 0);
        return Literal(value);
    }

    bool operator==(const Literal& other) const { return value == other.value; }

    bool operator!=(const Literal& other) const { return value != other.value; }

    Variable get_variable() const { return std::abs(value); }

    bool has_variable(Variable var) const { return var == std::abs(value); }

    VariablePhase phase() const
    {
        return (value < 0) ? VariablePhase::NEGATIVE : VariablePhase::POSITIVE;
    }

    bool is_negated() const { return value < 0; }

    Literal negated() const { return Literal(-value); }

    void negate() { value = -value; }

    Literal operator-() const { return Literal(-value); }

    friend std::ostream& operator<<(std::ostream& stream, const Literal& literal)
    {
        stream << literal.value;
        return stream;
    }

    // WARNING: do not use std::set for literals when their phase is important
    friend bool operator<(const Literal& left, const Literal& right)
    {
        return left.get_variable() < right.get_variable();
    }

    // if we want to use Literals in a hash table, we do need something
    // that can calculate hashes
    struct Hash
    {
        std::size_t operator()(const Literal& lit) const
        {
            return std::hash<int>{}(lit.get_variable());
        }
    };

    friend Clause;
};

enum class ClauseStatus
{
    SATISFIED,
    CONFLICT,
    UNDETERMINED
};

class Clause
{
    // On most 64 bit architectures, an odd number does not make sense here,
    // as unions are always padded
    //
    // In the LP64 memory model:
    //  * if the constant is 2, Clause has 16 bytes (8 fewer than a vector)
    //  * if the constant is 4, Clause has 24 bytes (the same as a vector)
    //
    // There is a tradeoff here. When the entire clause fits in place,
    // then access to its elements is faster - there is less indirection,
    // a lower chance of a cache miss. However, this makes the class itself
    // larger, which makes fewer clauses fit into cache, which introduces
    // a higher chance of a cache miss.
    //
    // In general, due to padding, this constant should always be even.
    // Anything less than 2 does not offer any advantage in LP64 - pointers
    // are 2 literals long. After a limited experimentation, 2 is quite
    // good, then it gets slower, and from ~20 onward it improves again,
    // at the cost of larger memory consumption
    static constexpr unsigned MAX_IN_PLACE = 2;

    // union cannot store complex types like vectors and unique
    // pointers, we have to use raw pointers
    using literal_vector = Literal*;
    using literal_array = std::array<Literal, MAX_IN_PLACE>;

    unsigned _size;
    union Body
    {
        literal_vector vec;
        literal_array arr;
        Body() {}
    } _body;

public:
    Clause(const std::vector<Literal>& literals)
        : _size(literals.size())
    {
        if (_size > MAX_IN_PLACE)
        {
            _body.vec = new Literal[_size];

            for (int i = 0; i < literals.size(); i++)
            {
                _body.vec[i] = literals[i];
            }
        }
        else
        {
            _body.arr = literal_array{};

            for (int i = 0; i < literals.size(); i++)
            {
                _body.arr[i] = literals[i];
            }
        }
    }

    Clause(Clause&& other) noexcept
        : _size(other._size)
    {
        if (_size > MAX_IN_PLACE)
        {
            _body.vec = other._body.vec;
        }
        else
        {
            _body.arr = other._body.arr;
        }

        other._size = 0;
        other._body.vec = nullptr;
    }

    Clause(const Clause& other)
        : _size(other._size)
    {
        if (_size > MAX_IN_PLACE)
        {
            _body.vec = new Literal[_size];

            for (int i = 0; i < _size; i++)
            {
                _body.vec[i] = other._body.vec[i];
            }
        }
        else
        {
            _body.arr = other._body.arr;
        }
    }

    Clause& operator=(Clause&& other) noexcept
    {
        this->~Clause();
        _size = other._size;

        if (_size > MAX_IN_PLACE)
        {
            _body.vec = other._body.vec;
        }
        else
        {
            _body.arr = other._body.arr;
        }

        other._size = 0;
        other._body.vec = nullptr;
        return *this;
    }

    Clause& operator=(const Clause& other)
    {
        this->~Clause();
        _size = other._size;

        if (_size > MAX_IN_PLACE)
        {
            _body.vec = new Literal[_size];

            for (int i = 0; i < _size; i++)
            {
                _body.vec[i] = other._body.vec[i];
            }
        }
        else
        {
            _body.arr = other._body.arr;
        }

        return *this;
    }

    ~Clause() noexcept
    {
        if (_size > MAX_IN_PLACE)
        {
            delete[] _body.vec;
        }
    }

    // only called after parsing - we can afford the expensive operations...
    static Clause from_numbers(const std::vector<int>& numbers)
    {
        std::vector<Literal> literals;

        std::transform(numbers.begin(), numbers.end(), std::back_inserter(literals),
                       Literal::from_int);

        std::sort(literals.begin(), literals.end());
        literals.erase(std::unique(literals.begin(), literals.end()), literals.end());

        return Clause(literals);
    }

    unsigned size() const { return _size; }

    void print() const
    {
        for (auto literal : *this)
        {
            std::cout << literal << ',';
        }
        std::cout << std::endl;
    }

    Literal& operator[](unsigned idx)
    {
        if (_size > MAX_IN_PLACE)
        {
            return _body.vec[idx];
        }
        else
        {
            assert(idx < _size);
            return _body.arr[idx];
        }
    }

    Literal operator[](unsigned idx) const
    {
        if (_size > MAX_IN_PLACE)
        {
            return _body.vec[idx];
        }
        else
        {
            assert(idx < _size);
            return _body.arr[idx];
        }
    }

    // we can finally iterate the clause directly
    Literal* begin()
    {
        if (_size > MAX_IN_PLACE)
        {
            return _body.vec;
        }
        else
        {
            return &_body.arr.front();
        }
    }

    Literal* end()
    {
        if (_size > MAX_IN_PLACE)
        {
            return _body.vec + _size;
        }
        else
        {
            return &_body.arr.front() + _size;
        }
    }

    const Literal* begin() const
    {
        if (_size > MAX_IN_PLACE)
        {
            return _body.vec;
        }
        else
        {
            return &_body.arr.front();
        }
    }

    const Literal* end() const
    {
        if (_size > MAX_IN_PLACE)
        {
            return _body.vec + _size;
        }
        else
        {
            return &_body.arr.front() + _size;
        }
    }
};

struct Formula
{
    std::vector<Clause> body;
    int num_variables;
    int num_clauses;

    Formula(int num_vars, int num_cs)
        : body{{}},
          num_variables{num_vars},
          num_clauses{num_cs}
    {
        body.reserve(num_clauses);
    }

    Formula(std::vector<Clause> b, int num_vars, int num_cs)
        : body{std::move(b)},
          num_variables{num_vars},
          num_clauses{num_cs}
    {
    }

    void add_clause(const Clause& clause)
    {
        body.push_back(clause);
        num_clauses++;
    }
};
