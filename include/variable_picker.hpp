#pragma once

#include "engine.hpp"
#include "formula.hpp"
#include "heap.hpp"
#include <memory>
#include <optional>

enum class VariablePickerType : char
{
    BASIC,
    EVSIDS
};

class VariablePicker
{
public:
    static std::unique_ptr<VariablePicker> choose(VariablePickerType type,
                                                  int variable_count);
    virtual std::optional<Variable> pick(const Engine& engine) = 0;

    // sends a clause that was used in the current conflict to increase activity
    // of its variables
    virtual void register_clause(const Clause&) = 0;

    // signals that the current conflict is completed
    // VariablePicker must update its internals accodringly
    virtual void finish_conflict_learning(const Engine& engine, int new_level) = 0;

    virtual ~VariablePicker() = default;
};

// picks the first unassigned variable
class BasicVariablePicker : public VariablePicker
{
public:
    std::optional<Variable> pick(const Engine& engine) override;
    void finish_conflict_learning(const Engine& engine, int new_level) override{};
    void register_clause(const Clause&) override{};
};

// implements EVSIDS algorithm
// heavily inspired by MiniSat
class EVSIDSVariablePicker : public VariablePicker
{
    // indices are variable ids
    // signifies if a variable already had its activity increased in the current conflict
    std::vector<bool> _seen_in_current_conflict;

    // priority queue for sorting variables
    // stores variables' activities
    Heap _heap;

    // values for activity increasing
    // this is added to variables' activity to increase it
    double _increase_by = 1.0;
    // after each conflict, multiply _increase_by by this
    const double _increase_factor = 1.01;
    // when _increase_by exceeds this, divide everything by this
    const double _reset_activity_threshold = 1e100;

    void increase_activity(Variable var);

public:
    EVSIDSVariablePicker(int variables_count = 0)
        : VariablePicker(),
          _seen_in_current_conflict(variables_count + 1, false),
          _heap(variables_count)
    {
    }

    std::optional<Variable> pick(const Engine& engine) override;
    void finish_conflict_learning(const Engine& engine, int new_level) override;
    void register_clause(const Clause&) override;
};
