#pragma once

#include "formula.hpp"

class Heap
{
    // priority queue
    std::vector<Variable> _heap;

    // incides are variables
    std::vector<double> _activities;

    // index behing the last element of priority queue
    // behind it are currently discarded values
    int _heap_end;

    // stores position of each variable in heap as index
    // indices are variables
    std::vector<int> _positions;

    // comparator for heap elements
    bool gt(Variable var1, Variable var2) const
    {
        return _activities[var1] > _activities[var2];
    }

    int left(int i) const { return 2 * i + 1; }

    int right(int i) const { return 2 * i + 2; }

    // suppose i has a parent (i > 0)
    int parent(int i) const
    {
        assert(i > 0);
        return (i - 1) / 2;
    }

    void swap(Variable var1, Variable var2)
    {
        std::swap(_heap[_positions[var1]], _heap[_positions[var2]]);
        std::swap(_positions[var1], _positions[var2]);
    }

    void heapify_down(int);
    void heapify_up(int);

public:
    Heap(int variables_count);

    Variable extract_max();
    Variable show_max()
    {
        assert(!empty());
        return _heap[0];
    }

    void increase_activity(Variable, double);
    void insert(Variable);

    bool empty() const { return _heap_end == 0; }

    int size() const { return _heap_end; }

    void decrease_all_activities(double);

    double get_activity(Variable var) { return _activities[var]; }
};