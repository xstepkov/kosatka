#pragma once

#include "engine.hpp"
#include <memory>
#include <vector>

enum class ForgettingManagerType : char
{
    NO,
    VSIDS
};

class ForgettingManager
{
public:
    static std::unique_ptr<ForgettingManager> choose(ForgettingManagerType type,
                                                     int num_clauses,
                                                     int forgetting_constant,
                                                     double remove_percentage);
    virtual void bump_clause_activity(Engine::ClauseIdx idx) = 0;
    virtual void clause_added() = 0;
    virtual void register_conflict() = 0;
    virtual bool time_to_forget() const = 0;

    virtual const std::vector<Engine::ClauseIdx>& clauses_to_forget() const = 0;
    virtual void
    register_restructuring(const std::vector<Engine::ClauseIdx>& relocations) = 0;

    virtual ~ForgettingManager() = default;
};

class NoForgettingManager : public ForgettingManager
{
public:
    void bump_clause_activity(Engine::ClauseIdx idx) override;
    void clause_added() override;
    void register_conflict() override;
    bool time_to_forget() const override;

    const std::vector<Engine::ClauseIdx>& clauses_to_forget() const override;
    virtual void
    register_restructuring(const std::vector<Engine::ClauseIdx>& relocations) override;
};

// there is some duplication with the VariablePicker; but it was easier
// to write it from the bottom up; it can probably be made more general
//
// IMPORTANT: The engine is not obligated to remove every clause
// that the forgetting manager recommends - in particular, the solver
// never removes clauses which have one or two literals, as they are
// cheap to unit-propagate and often either important or already assigned
class VSIDSForgettingManager : public ForgettingManager
{
    std::vector<Engine::ClauseIdx> _heap = {};

    // where in the heap each clause is located; indices are
    // ClauseIdxs - original_clause_count
    std::vector<int> _locations = {};

    // the indices are ClauseIdxs - original_clause_count
    std::vector<double> _activities = {};

    double _increase_by = 1.0;

    // newly added clauses will get the highest current priority
    double max_activity = 1.0;

    static constexpr double _increase_factor = 1.01;
    static constexpr double _reset_activity_threshold = 1e100;

    // this class does two things - select what should be forgotten,
    // and when it is a good time to forget; better decomposition is
    // possible, but nah
    const int forgetting_constant;
    int conflicts = 0;

    // the percentage of clauses that are to be forgotten when forgetting
    // takes place
    double _keep_percentage = 0.5;

    // this class only works with learnt clauses, it needs to know
    // which are their indices
    const int original_clause_count;

public:
    VSIDSForgettingManager(int num_clauses, int forgetting_constant,
                           double remove_percentage);
    void bump_clause_activity(Engine::ClauseIdx idx) override;
    void clause_added() override;
    void register_conflict() override;
    bool time_to_forget() const override;

    const std::vector<Engine::ClauseIdx>& clauses_to_forget() const override;
    virtual void
    register_restructuring(const std::vector<Engine::ClauseIdx>& relocations) override;

private:
    void heapify_up(int heap_idx);
    void decrease_all_activities();
    double& activity(Engine::ClauseIdx idx);
    int& location(Engine::ClauseIdx idx);
};
