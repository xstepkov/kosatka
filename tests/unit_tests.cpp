#include "formula.hpp"
#include "heap.hpp"
#include "restart_manager.hpp"
#include "threeval.hpp"
#include <cassert>

const VariablePhase POSITIVE = VariablePhase::POSITIVE;
const VariablePhase NEGATIVE = VariablePhase::NEGATIVE;

static void test_literal()
{
    Literal five_true = Literal::from_variable(5, POSITIVE);
    assert(five_true.get_variable() == 5);
    assert(!five_true.is_negated());

    Literal hundred_false = Literal::from_variable(100, NEGATIVE);
    assert(hundred_false.get_variable() == 100);
    assert(hundred_false.is_negated());

    five_true.negate();
    assert(five_true.is_negated());

    hundred_false.negate();
    assert(!hundred_false.is_negated());

    auto five_true_again = five_true.negated();
    assert(five_true_again.get_variable() == 5);
    assert(!five_true_again.is_negated());

    five_true_again.negate();

    assert(five_true_again == five_true);
    assert(five_true_again != hundred_false);
}

static void test_assignment_state()
{
    AssignmentState state = AssignmentState();

    assert(state.is_unknown());
    assert(!state.is_decided());
    assert(!state.is_true());
    assert(!state.is_false());

    state.flip();

    assert(state.is_unknown());
    assert(!state.is_decided());
    assert(!state.is_true());
    assert(!state.is_false());

    state.assign(true);

    assert(!state.is_unknown());
    assert(state.is_decided());
    assert(state.is_true());
    assert(!state.is_false());

    state = AssignmentState::FALSE;

    assert(!state.is_unknown());
    assert(state.is_decided());
    assert(!state.is_true());
    assert(state.is_false());

    state.unassign();

    assert(state.is_unknown());
    assert(!state.is_decided());
    assert(!state.is_true());
    assert(!state.is_false());
}

/*static void test_formula_tracker()
{
    Formula f =
        Formula({Clause::from_numbers({-1, -2}), Clause::from_numbers({1, -2})}, 2, 2);
    FormulaTracker tracker(f);

    assert(tracker.clause_count() == 2);
    assert(tracker.variable_count() == 2);

    auto idx = tracker.learn(Clause::from_numbers({-2}));
    assert(tracker.clause_count() == 3);

    assert(tracker.is_learnt(idx));

    tracker.reset();

    assert(tracker.clause_count() == 2);

    int count_iters = 0;

    for (FormulaTracker::ClauseIdx idx : tracker.iterate_clause_indices())
    {
        assert(!tracker.is_learnt(idx));
        count_iters++;
    }

    assert(count_iters == 2);
}*/

/*void test_assignment_tracker()
{
    Formula f = Formula({Clause::from_numbers({-1, -2}), Clause::from_numbers({1, -2}),
                         Clause::from_numbers({2, 3}), Clause::from_numbers({4})},
                        4, 4);

    std::shared_ptr<FormulaTracker> ft = std::make_shared<FormulaTracker>(f);

    AssignmentTracker a_tracker(ft);

    assert(a_tracker.status() == AssignmentTracker::Status::UNKNOWN);
    assert(a_tracker.variable_status(1).is_unknown());
    assert(a_tracker.clause_status(1).is_unknown());
    assert(a_tracker.literal_status(Literal::from_variable(2, POSITIVE)).is_unknown());
    assert(!a_tracker.can_backtrack());
    assert(a_tracker.status() == AssignmentTracker::Status::UNKNOWN);
    assert(a_tracker.current_decision_level() == 0);

    a_tracker.unit_propagate();
    assert(a_tracker.variable_status(4).is_true());
    assert(a_tracker.trail().size() == 1);
    assert(a_tracker.reason(4) == 3);
    assert(a_tracker.decision_level(4) == 0);
    assert(a_tracker.current_decision_level() == 0);

    a_tracker.decide(1, POSITIVE);
    assert(a_tracker.variable_status(1).is_true());
    assert(a_tracker.literal_status(Literal::from_variable(1, POSITIVE)).is_true());
    assert(a_tracker.literal_status(Literal::from_variable(1, NEGATIVE)).is_false());
    assert(a_tracker.clause_status(1).is_true());
    assert(a_tracker.clause_status(0).is_unknown());
    assert(a_tracker.can_backtrack());
    assert(a_tracker.status() == AssignmentTracker::Status::UNKNOWN);
    assert(a_tracker.current_decision_level() == 1);
    assert(a_tracker.decision_level(1) == 1);
    assert(a_tracker.decision_level(4) == 0);

    a_tracker.unit_propagate();
    assert(a_tracker.trail().size() == 4);
    assert(a_tracker.variable_status(1).is_true());
    assert(a_tracker.variable_status(2).is_false());
    assert(a_tracker.variable_status(3).is_true());
    assert(a_tracker.status() == AssignmentTracker::Status::SOLVED);
    assert(a_tracker.can_backtrack());
    assert(a_tracker.current_decision_level() == 1);
    assert(a_tracker.decision_level(1) == 1);
    assert(a_tracker.decision_level(2) == 1);
    assert(a_tracker.decision_level(3) == 1);
    assert(a_tracker.decision_level(4) == 0);

    a_tracker.backtrack_to(1);
    assert(a_tracker.trail().size() == 4);

    a_tracker.backtrack_to(0);
    assert(a_tracker.trail().size() == 1);
    assert(a_tracker.variable_status(4).is_decided());

    a_tracker.decide(1, POSITIVE);
    a_tracker.unit_propagate();
    a_tracker.flip_last_decision();

    assert(a_tracker.variable_status(1).is_false());
    assert(a_tracker.trail().size() == 2);
    assert(!a_tracker.can_backtrack());

    a_tracker.unit_propagate();
    assert(a_tracker.variable_status(1).is_false());
    assert(a_tracker.variable_status(2).is_false());
    assert(a_tracker.variable_status(3).is_true());

    a_tracker.reset();
    assert(a_tracker.current_decision_level() == 0);
    a_tracker.decide(1, NEGATIVE);
    a_tracker.decide(2, POSITIVE);
    assert(a_tracker.status() == AssignmentTracker::Status::CONFLICT);
}*/

static void test_heap()
{
    Heap heap(5);
    assert((heap.size() == 5));

    auto max = heap.extract_max();
    assert(max == 1);
    assert(heap.size() == 4);

    heap.insert(1);
    std::cout << "size:" << heap.size() << '\n';
    assert(heap.size() == 5);

    heap.increase_activity(5, 1.5);
    assert(heap.show_max() == 5);
    heap.increase_activity(4, 3);
    assert(heap.show_max() == 4);
    heap.increase_activity(3, 2);
    assert(heap.show_max() == 4);
    heap.increase_activity(2, 4);
    assert(heap.show_max() == 2);

    max = heap.extract_max();
    assert(max == 2);
    max = heap.extract_max();
    assert(max == 4);
    max = heap.extract_max();
    assert(max == 3);
    max = heap.extract_max();
    assert(max == 5);
    max = heap.extract_max();
    assert(max == 1);
    assert(heap.empty());

    heap.insert(1);
    assert(!heap.empty());
    assert(heap.show_max() == 1);
    heap.insert(1);
    assert(!heap.empty());
    assert(heap.show_max() == 1);
    assert(heap.size() == 1);

    heap.decrease_all_activities(2.0);
    assert(heap.get_activity(4) == 1.5);
    assert(heap.get_activity(3) == 1.0);
    assert(heap.get_activity(2) == 2.0);
}

void test_const_restarts(int k)
{
    ConstantRestartManager const_restart(k);
    for (int i = 1; i <= k * 10; ++i)
    {
        const_restart.register_conflict();
        assert(const_restart.should_restart() == (i % k == 0));
    }
}

void test_luby_restarts(int k, const std::vector<int>& luby_seq)
{
    LubyRestartManager luby_restart(k);

    for (int i = 0; i < luby_seq.size(); ++i)
    {
        for (int j = 0; j < luby_seq[i] * k; ++j)
        {
            assert(!luby_restart.should_restart());
            luby_restart.register_conflict();
        }
        assert(luby_restart.should_restart());
    }
}

void test_restarts()
{
    LubySequenceGenerator generator;
    std::vector<int> luby_seq = {1,  1, 2, 1, 1, 2, 4,  1,  1, 2, 4, 8, 1, 1,  2,  4, 8,
                                 16, 1, 1, 2, 4, 8, 16, 32, 1, 1, 2, 4, 8, 16, 32, 64};

    for (int i = 0; i < luby_seq.size(); ++i)
    {
        int next = generator.next();
        assert(luby_seq[i] == next);
    }

    test_luby_restarts(1, luby_seq);
    test_luby_restarts(3, luby_seq);
    test_luby_restarts(10, luby_seq);

    test_const_restarts(1);
    test_const_restarts(3);
    test_const_restarts(10);
}

void test_clause()
{
    std::vector<Literal> literals = { Literal::from_int(1), Literal::from_int(2) };
}

int main(int argc, char** argv)
{
    assert(argc == 2);

    std::string arg(argv[1]);

    if (arg == "literal")
    {
        test_literal();
    }
    else if (arg == "assignment_state")
    {
        test_assignment_state();
    }
    else if (arg == "heap")
    {
        test_heap();
    }
    else if (arg == "restarts")
    {
        test_restarts();
    }
    else
    {
        assert(false);
    }

    return 0;
}
