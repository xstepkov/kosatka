#!/bin/bash

if ! command -v curl &> /dev/null
then
    echo "curl not found!"
    exit 1
fi

if ! command -v tar &> /dev/null
then
    echo "tar not found!"
    exit 1
fi

benchmarks_url_prefix="https://www.cs.ubc.ca/~hoos/SATLIB/Benchmarks/SAT/RND3SAT/"
benchmarks_dir="$(dirname $0)"

# we do the sat and the unsat benchmarks separately, because they are ziped differently (-:

sat_names=("uf50-218" "uf100-430") # maybe add other benchmarks later

for name in "${sat_names[@]}"; do
    prefix="${benchmarks_dir}/${name}"
    mkdir "${prefix}"
    curl -o "${prefix}.tar.gz" "${benchmarks_url_prefix}${name}.tar.gz"
    tar -xzf "${prefix}.tar.gz" -C "${prefix}"
    rm "${prefix}.tar.gz"
done

unsat_names=("uuf50-218" "uuf100-430")

for name in "${unsat_names[@]}"; do
    prefix="${benchmarks_dir}/${name}"
    curl -o "${prefix}.tar.gz" "${benchmarks_url_prefix}${name}.tar.gz"
    tar -xzf "${prefix}.tar.gz" -C "${benchmarks_dir}"
    rm "${prefix}.tar.gz"
done

mv "${benchmarks_dir}/UUF100.430.1000" "${benchmarks_dir}/uuf100-430"
mv "${benchmarks_dir}/UUF50.218.1000" "${benchmarks_dir}/uuf50-218"

exit 0
