# Benchmarks

In order to test that **koSATka** behaves correctly and analyse its resource
usage on many different formulas, we use benchmarks available on this
(website)[https://www.cs.ubc.ca/~hoos/SATLIB/benchm.html].

Because we do not own these benchmarks, and because they may take too much
space, they are not directly available in our repository. Instead, we provide
a script to download these benchmarks. These benchmarks are necessary to run
some of our tests.

The script requires `tar` and `curl` to be available on the system.
