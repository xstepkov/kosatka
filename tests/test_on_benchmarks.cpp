#include <cassert>
#include <filesystem>
#include <iostream>
#include <string>

#include "parser.hpp"
#include "solver.hpp"
#include "utilities/resource.hpp"
#include "validator.hpp"

constexpr Solver::Options options = {
    Solver::SolverAlgorithm::CDCL, true, VariablePickerType::EVSIDS,
    PhasePickerType::SAVING, RestartManagerType::LUBY, 80,
    ForgettingManagerType::VSIDS, 1000, 0.66, true
};

// we assume that all files in a directory are either satisfiable
// or unsatisfiable
template <bool expects_satisfiable>
void run_on_directory(std::string dir, long first_n = -1)
{
    namespace fs = std::filesystem;

    if (!fs::exists(dir) || !fs::is_directory(dir))
    {
        std::cerr << "Benchmarks not downloaded!\n"
                  << "Use the script provided in"
                  << "kosatka/tests/benchmarks/download.sh\n";
        assert(false);
    }

    auto solver_tracker = TimeUsageTracker::create_paused();
    auto parser_tracker = TimeUsageTracker::create_paused();
    auto validator_tracker = TimeUsageTracker::create_paused();

    for (const auto& entry : fs::directory_iterator(dir))
    {
        if (first_n-- == 0) // we may not want to run all the benchmarks
        {
            break;
        }

        if (!fs::is_regular_file(entry) || entry.path().extension() != ".cnf")
        {
            continue;
        }

        parser_tracker.unpause();
        auto parse_res = parse_formula(entry.path());
        parser_tracker.pause();

        if (!parse_res.has_value())
        {
            std::cerr << "Failed to parse formula in: " << entry.path().string() << '\n';
            assert(false);
        }

        auto formula = parse_res.value();
        solver_tracker.unpause();
        Solver solver{formula, options};
        auto res = solver.run();
        solver_tracker.pause();

        if constexpr (expects_satisfiable)
        {
            // maybe also report unknowns???
            if (res == SolverResult::UNKNOWN)
            {
                continue;
            }

            if (res == SolverResult::UNSAT)
            {
                std::cerr << "Expected SAT, got UNSAT in: " << entry.path().string()
                          << '\n';
                assert(false);
            }

            auto model = solver.get_model();
            validator_tracker.unpause();
            auto validation_res = Validator{formula}.validate(model);
            validator_tracker.pause();

            if (!validation_res)
            {
                // maybe also return the model???
                std::cerr << "Validator rejected a model in: " << entry.path().string()
                          << '\n';
                std::cerr << "Model size: " << model.size() << std::endl;
                std::cerr << "Model: ";

                for (auto lit : model)
                {
                    std::cout << lit << ',';
                }

                std::cout << std::endl;

                assert(false);
            }
        }

        else if (res == SolverResult::SAT)
        {
            std::cerr << "Expected UNSAT, got SAT in: " << entry.path().string() << '\n';
            assert(false);
        }
    }

    constexpr auto msec = TimeUsage::Unit::MSEC;

    std::cout << "Parser took: " << parser_tracker.get_usage().total_time<msec>()
              << " ms\n";

    std::cout << "Solver took: " << solver_tracker.get_usage().total_time<msec>()
              << " ms\n";

    std::cout << "Validator took: " << validator_tracker.get_usage().total_time<msec>()
              << " ms\n";
}

int main(int argc, char** argv)
{
    if (argc == 1)
    {
        return 0;
    }

    if (std::string(argv[1]) == "uf50-218")
    {
        // the hardcoded paths are ugly, but it should work
        run_on_directory<true>("../../tests/benchmarks/uf50-218", 1000);
    }

    else if (std::string(argv[1]) == "uuf50-218")
    {
        run_on_directory<false>("../../tests/benchmarks/uuf50-218", 1000);
    }

    else if (std::string(argv[1]) == "uf100-430")
    {
        run_on_directory<true>("../../tests/benchmarks/uf100-430", 1000);
    }

    else if (std::string(argv[1]) == "uuf100-430")
    {
        run_on_directory<false>("../../tests/benchmarks/uuf100-430", 1000);
    }

    return 0;
}
