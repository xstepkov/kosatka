#include "forgetting_manager.hpp"

std::unique_ptr<ForgettingManager> ForgettingManager::choose(ForgettingManagerType type,
                                                             int num_clauses,
                                                             int forgetting_constant,
                                                             double forget_percentage)
{
    switch (type)
    {
    case ForgettingManagerType::NO:
        return std::make_unique<NoForgettingManager>();
    case ForgettingManagerType::VSIDS:
        return std::make_unique<VSIDSForgettingManager>(num_clauses, forgetting_constant,
                                                        forget_percentage);
    default:
        assert(false);
    }
}

void NoForgettingManager::bump_clause_activity(Engine::ClauseIdx) {}
void NoForgettingManager::clause_added() {}
void NoForgettingManager::register_conflict() {}
bool NoForgettingManager::time_to_forget() const { return false; }

const std::vector<Engine::ClauseIdx>& NoForgettingManager::clauses_to_forget() const
{
    static const std::vector<Engine::ClauseIdx> empty = {};
    return empty;
}

void NoForgettingManager::register_restructuring(const std::vector<Engine::ClauseIdx>&) {}

VSIDSForgettingManager::VSIDSForgettingManager(int num_clauses, int forgetting_constant,
                                               double forget_percentage)
    : forgetting_constant(forgetting_constant),
      original_clause_count(num_clauses),
      _keep_percentage(1.0 - forget_percentage)
{
}

void VSIDSForgettingManager::bump_clause_activity(Engine::ClauseIdx idx)
{
    // we do not do anything with the original clauses
    if (idx < original_clause_count)
    {
        return;
    }

    double new_activity = _activities[idx - original_clause_count] + _increase_by;

    if (new_activity > _reset_activity_threshold)
    {
        _increase_by /= _reset_activity_threshold;
        decrease_all_activities();
        new_activity = _activities[idx - original_clause_count] + _increase_by;
    }

    _activities[idx - original_clause_count] = new_activity;
    max_activity = std::max(max_activity, new_activity);

    heapify_up(_locations[idx - original_clause_count]);
}

void VSIDSForgettingManager::clause_added()
{
    Engine::ClauseIdx idx = _heap.size() + original_clause_count;

    _heap.push_back(idx);
    _activities.push_back(max_activity);
    _locations.push_back(_heap.size() - 1);
    heapify_up(_heap.size() - 1);
}

void VSIDSForgettingManager::register_conflict()
{
    conflicts = std::min(INT_MAX, conflicts + 1);
}

bool VSIDSForgettingManager::time_to_forget() const
{
    return conflicts >= forgetting_constant;
}

// the lower half
const std::vector<Engine::ClauseIdx>& VSIDSForgettingManager::clauses_to_forget() const
{
    // again, this is static, so be careful with threads, concurrent instances
    // of solver etc.
    static std::vector<Engine::ClauseIdx> to_forget;
    to_forget.clear();

    to_forget.reserve(_heap.size() / 2);

    for (int i = 1 + (int)(_keep_percentage * _heap.size()); i < _heap.size(); i++)
    {
        to_forget.push_back(_heap[i]);
    }

    return to_forget;
}

void VSIDSForgettingManager::register_restructuring(
    const std::vector<Engine::ClauseIdx>& relocations)
{
    static std::vector<Engine::ClauseIdx> flipped_relocations(relocations.size(), -1);
    flipped_relocations.clear();

    std::fill_n(std::back_inserter(flipped_relocations), relocations.size(), -1);

    for (Engine::ClauseIdx old_idx = 0; old_idx < relocations.size(); old_idx++)
    {
        if (relocations[old_idx] != -1)
        {
            flipped_relocations[relocations[old_idx]] = old_idx;
        }
    }

    std::vector<double> old_activities = std::move(_activities);

    _heap = {};
    _locations = {};
    _activities = {};

    conflicts = 0;

    for (auto idx = original_clause_count; idx < flipped_relocations.size(); idx++)
    {
        // it must hold that the indices that are now empty are at the end of the formula
        if (flipped_relocations[idx] == -1)
        {
            break;
        }

        _heap.push_back(idx);
        _activities.push_back(
            old_activities[flipped_relocations[idx - original_clause_count]]);
        _locations.push_back(_heap.size() - 1);
        heapify_up(_heap.size() - 1);
    }
}

static int parent(int heap_idx) { return (heap_idx - 1) / 2; }

void VSIDSForgettingManager::heapify_up(int heap_idx)
{
    while (heap_idx != 0 && activity(_heap[parent(heap_idx)]) < activity(_heap[heap_idx]))
    {
        std::swap(location(_heap[heap_idx]), location(_heap[parent(heap_idx)]));
        std::swap(_heap[heap_idx], _heap[parent(heap_idx)]);
        heap_idx = parent(heap_idx);
    }
}

void VSIDSForgettingManager::decrease_all_activities()
{
    for (double& activity : _activities)
    {
        activity /= _reset_activity_threshold;
    }
}

double& VSIDSForgettingManager::activity(Engine::ClauseIdx idx)
{
    return _activities[idx - original_clause_count];
}

int& VSIDSForgettingManager::location(Engine::ClauseIdx idx)
{
    return _locations[idx - original_clause_count];
}
