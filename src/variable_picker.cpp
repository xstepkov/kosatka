#include "variable_picker.hpp"
#include <cassert>

std::unique_ptr<VariablePicker> VariablePicker::choose(VariablePickerType type,
                                                       int variable_count)
{
    switch (type)
    {
    case VariablePickerType::BASIC:
        return std::make_unique<BasicVariablePicker>();
    case VariablePickerType::EVSIDS:
        return std::make_unique<EVSIDSVariablePicker>(variable_count);
    default:
        assert(false);
    }
}

std::optional<Variable> BasicVariablePicker::pick(const Engine& engine)
{
    // A for loop here is possible, but much less readable in my opinion

    Variable candidate = engine.last_decision().value_or(0) + 1;

    while (candidate <= engine.variable_count())
    {
        if (engine.variable_status(candidate).is_unknown())
        {
            return candidate;
        }

        candidate++;
    }

    return std::nullopt;
}

std::optional<Variable> EVSIDSVariablePicker::pick(const Engine& engine)
{
    if (_heap.empty())
    {
        return std::nullopt;
    }

    Variable candidate_var = _heap.extract_max();
    while (!_heap.empty() && engine.variable_status(candidate_var).is_decided())
    {
        candidate_var = _heap.extract_max();
    }

    if (engine.variable_status(candidate_var).is_decided())
    {
        return std::nullopt;
    }
    return candidate_var;
}

void EVSIDSVariablePicker::increase_activity(Variable var)
{
    double new_act = _heap.get_activity(var) + _increase_by;
    if (new_act > _reset_activity_threshold)
    {
        _increase_by /= _reset_activity_threshold;
        _heap.decrease_all_activities(_reset_activity_threshold);
        new_act = _heap.get_activity(var) + _increase_by;
    }

    _heap.increase_activity(var, new_act);
}

void EVSIDSVariablePicker::finish_conflict_learning(const Engine& engine, int new_level)
{
    std::fill(_seen_in_current_conflict.begin(), _seen_in_current_conflict.end(), false);
    _increase_by *= _increase_factor;

    // put all backtracked variables back into queue
    for (const auto& var : engine.unassigned_in_last_backtrack())
    {
        _heap.insert(var);
    }
}

void EVSIDSVariablePicker::register_clause(const Clause& clause)
{
    for (const auto& lit : clause)
    {
        auto var = lit.get_variable();
        assert(var < _seen_in_current_conflict.size());
        if (!_seen_in_current_conflict[var])
        {
            _seen_in_current_conflict[var] = true;
            increase_activity(var);
        }
    }
}
