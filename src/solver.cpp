#include <cassert>
#include <optional>
#include <unordered_set>

#include "solver.hpp"

using ClauseIdx = Engine::ClauseIdx;
using TrailIdx = Engine::TrailIdx;

Solver::Solver(Formula formula, Options options)
    : _engine(std::move(formula)),
      _algorithm(options.algorithm),
      _minimize(options.minimize),
      _variable_picker(
          VariablePicker::choose(options.variable_picker_type, _engine.variable_count())),
      _phase_picker(PhasePicker::choose(options.phase_picker_type)),
      _restart_manager(
          RestartManager::choose(options.restart_manager_type, options.restart_constant)),
      _forgetting_manager(ForgettingManager::choose(
          options.forgetting_manager_type, _engine.clause_count(),
          options.forgetting_constant, options.forget_percentage)),
      _minimize_model(options.minimize_model)
{
}

// rewritten to work similarly to minisat
Clause Solver::learn_from_conflict(ClauseIdx conflict_clause_idx) const
{
    assert(_engine.status() == Engine::Status::CONFLICT);

    _forgetting_manager->bump_clause_activity(conflict_clause_idx);

    // WARNING: This vector is static; if the number of variables changes
    // during execution, this will BREAK! Solution: add a call to resize,
    // should not be to expensive. Also, if we start using threads, it
    // should become thread_only, not static
    //
    // Invariant: in all paths in this method, when the method finishes, all variables
    // in the vector will be marked as not seen
    static std::vector<bool> seen(_engine.variable_count() + 1, false);
    Literal asserting_literal = _engine.trail().back();

    // This is also static, but as long as we do not use multiple threads
    // multiple instances of Solver, it should be fine
    static std::vector<Literal> learnt_literals;
    learnt_literals.clear();

    int variables_at_current_decision_level = 0;
    const Clause& conflict_clause = _engine.get_clause(conflict_clause_idx);

    for (int i = 0; i < conflict_clause.size(); i++)
    {
        Literal literal = conflict_clause[i];
        seen[literal.get_variable()] = true;
        if (_engine.at_current_decision_level(literal.get_variable()))
        {
            variables_at_current_decision_level++;
        }
        else
        {
            learnt_literals.push_back(literal);
        }
    }

    for (int i = _engine.trail().size() - 1; i >= 0; i--)
    {
        asserting_literal = _engine.trail()[i];

        // not seen = not relevant to the current conflict
        if (!seen[asserting_literal.get_variable()])
        {
            continue;
        }

        // we have found a UIP
        if (variables_at_current_decision_level == 1)
        {
            learnt_literals.push_back(asserting_literal.negated());
            break;
        }

        auto reason_idx = _engine.reason(asserting_literal.get_variable());
        assert(reason_idx != Engine::NO_REASON); // there must be a UIP

        if (reason_idx != Engine::UNIT_LITERAL)
        {
            // the literal is removed, then added, and then remove again
            seen[asserting_literal.get_variable()] = false;
            variables_at_current_decision_level--;

            const Clause& reason_clause = _engine.get_clause(reason_idx);

            _forgetting_manager->bump_clause_activity(reason_idx);
            _variable_picker->register_clause(reason_clause);

            // this is basically resolution
            for (Literal literal : reason_clause)
            {
                if (seen[literal.get_variable()])
                {
                    continue;
                }

                seen[literal.get_variable()] = true;
                if (_engine.at_current_decision_level(literal.get_variable()))
                {
                    variables_at_current_decision_level++;
                }
                else
                {
                    learnt_literals.push_back(literal);
                }
            }
        }

        seen[asserting_literal.get_variable()] = false;
        variables_at_current_decision_level--;
    }

    if (_minimize)
    {
        static std::vector<Literal> learnt_minimized;
        learnt_minimized.reserve(learnt_literals.size());
        learnt_minimized.clear();

        for (Literal learnt_literal : learnt_literals)
        {
            ClauseIdx reason_idx = _engine.reason(learnt_literal.get_variable());
            bool keep = true;

            switch (reason_idx)
            {
            case Engine::NO_REASON:
                break;
            case Engine::UNIT_LITERAL:
                keep = false;
                break;
            default:
                const Clause& reason = _engine.get_clause(reason_idx);
                keep = false;

                for (Literal reason_literal : reason)
                {
                    if (!seen[reason_literal.get_variable()])
                    {
                        keep = true;
                        break;
                    }
                }
            }

            if (keep)
            {
                learnt_minimized.push_back(learnt_literal);
            }
        }

        for (Literal literal : learnt_literals)
        {
            seen[literal.get_variable()] = false;
        }

        std::swap(learnt_literals, learnt_minimized);
    }
    else
    {
        for (Literal literal : learnt_literals)
        {
            seen[literal.get_variable()] = false;
        }
    }

    return Clause(learnt_literals);
}

SolverResult Solver::run_DPLL()
{
    if (_engine.unit_propagate().has_value())
    {
        return SolverResult::UNSAT;
    }

    bool should_decide = true;

    while (true)
    {
        // for DPLL, restarts do not make much sense, but whatever
        if (_restart_manager->should_restart())
        {
            _engine.reset();
        }

        if (should_decide)
        {
            auto maybe_variable = _variable_picker->pick(_engine);

            if (!maybe_variable.has_value())
            {
                break;
            }

            Variable var = maybe_variable.value();
            VariablePhase phase = _phase_picker->pick(var, _engine);

            _engine.decide(var, phase);
        }

        if (_engine.unit_propagate().has_value())
        {
            if (!_engine.can_backtrack())
            {
                return SolverResult::UNSAT;
            }

            _engine.flip_last_decision();
            should_decide = false;
        }
        else
        {
            should_decide = true;
        }
    }

    return SolverResult::SAT;
}

SolverResult Solver::run_CDCL()
{
    auto maybe_conflict_clause = _engine.unit_propagate();

    if (maybe_conflict_clause.has_value())
    {
        return SolverResult::UNSAT;
    }

    while (true)
    {
        auto maybe_variable = _variable_picker->pick(_engine);

        if (!maybe_variable.has_value())
        {
            break;
        }

        Variable var = maybe_variable.value();
        VariablePhase phase = _phase_picker->pick(var, _engine);

        _engine.decide(var, phase);

        maybe_conflict_clause = _engine.unit_propagate();

        while (maybe_conflict_clause.has_value())
        {
            if (!_engine.can_backtrack())
            {
                return SolverResult::UNSAT;
            }

            Clause learnt = learn_from_conflict(maybe_conflict_clause.value());

            // we can always unit propagate the learnt clause somewhere
            auto new_level = _engine.lowest_unit_propagation_level(learnt).value();

            // this way we restart right after the number of conflicts is reached
            if (_restart_manager->should_restart())
            {
                _engine.reset();

                if (_forgetting_manager->time_to_forget())
                {
                    const auto& to_forget = _forgetting_manager->clauses_to_forget();
                    const auto& relocations = _engine.forget(to_forget);
                    _forgetting_manager->register_restructuring(relocations);
                }
            }
            else
            {
                _engine.backtrack_to(new_level);
            }

            if (_engine.learn(std::move(learnt)))
            {
                _forgetting_manager->clause_added();
            }

            _restart_manager->register_conflict();
            _forgetting_manager->register_conflict();

            _variable_picker->finish_conflict_learning(_engine,
                                                       _engine.current_decision_level());

            maybe_conflict_clause = _engine.unit_propagate();
        }
    }

    return SolverResult::SAT;
}

SolverResult Solver::run()
{
    if (_engine.is_trivially_unsat())
    {
        return SolverResult::UNSAT;
    }

    switch (_algorithm)
    {
    case SolverAlgorithm::CDCL:
        return run_CDCL();
    case SolverAlgorithm::DPLL:
        return run_DPLL();
    }
}

std::vector<Literal> Solver::get_model() const
{
    assert(_engine.status() == Engine::Status::SOLVED);

    if (!_minimize_model)
    {
        return _engine.trail();
    }

    // a very basic algorithm, go through clauses, and if you find that
    // the clause is not yet true, pick a literal that is true in the
    // current assignment, and add it to the model
    std::unordered_set<Literal, Literal::Hash> model;

    for (Literal lit : _engine.trail())
    {
        if (_engine.reason(lit.get_variable()) != Engine::NO_REASON)
        {
            // unit propagated variables must always be in the model - they happened,
            // because the other assignment would lead to a contradiction
            model.insert(lit);
        }
    }

    for (ClauseIdx idx = 0; idx < _engine.original_clause_count(); idx++)
    {
        bool is_true_in_current_model = false;
        for (Literal lit : _engine.get_clause(idx))
        {
            if (model.find(lit) != model.end())
            {
                is_true_in_current_model = true;
                break;
            }
        }

        if (is_true_in_current_model)
        {
            continue;
        }

        for (Literal lit : _engine.get_clause(idx))
        {
            // this must happen at least once, otherwise,
            // the formula would not be SAT
            if (_engine.literal_status(lit).is_true())
            {
                model.insert(lit);
                break;
            }
        }
    }

    return std::vector(model.begin(), model.end());
}
