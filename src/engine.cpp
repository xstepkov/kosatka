#include "engine.hpp"
#include <algorithm>

Engine::ClauseInfo::ClauseInfo(Clause clause)
    : clause(std::move(clause))
{
}

Engine::Engine(Formula formula)
    : _variable_infos(formula.num_variables + 1, VariableInfo()),
      _variable_states(formula.num_variables + 1),
      _original_clause_count(formula.num_clauses)
{
    _clause_infos.reserve(formula.num_clauses);
    _decisions.reserve(formula.num_variables);
    _trail.reserve(formula.num_variables);
    _unassigned_in_last_backtrack.reserve(formula.num_variables);

    for (Clause& clause : formula.body)
    {

        if (clause.size() == 1)
        {
            Literal lit = clause[0];

            if (literal_status(lit).is_false())
            {
                _trivial_unsat = true;
            }

            set_literal_to_true(lit);
            _variable_infos[lit.get_variable()].reason = UNIT_LITERAL;
            _original_clause_count--;
        }
        else
        {
            _clause_infos.emplace_back(std::move(clause));
            set_up_watch(_clause_infos.size() - 1, true);
            set_up_watch(_clause_infos.size() - 1, false);
        }
    }
}

const std::vector<Literal>& Engine::trail() const { return _trail; }

int Engine::variable_count() const { return _variable_infos.size() - 1; }

int Engine::clause_count() const { return _clause_infos.size(); }

int Engine::original_clause_count() const { return _original_clause_count; }

const Clause& Engine::get_clause(ClauseIdx idx) const
{
    assert(idx >= 0);
    assert(idx <= _clause_infos.size());
    return _clause_infos[idx].clause;
}

AssignmentState Engine::clause_status(ClauseIdx idx) const
{
    assert(_next_unit_propagation == _trail.size());
    const Clause& clause = get_clause(idx);

    // to avoid having to check where the clause begins every time
    auto clause_body = clause.begin();

    if (literal_status(clause_body[0]).is_false() &&
        literal_status(clause_body[1]).is_false())
    {
        // if both watched literals are false, then the whole clause is false - otherwise,
        // they would have been reassigned
        return AssignmentState::FALSE;
    }

    if (literal_status(clause_body[0]).is_true() ||
        literal_status(clause_body[1]).is_true())
    {
        return AssignmentState::TRUE;
    }

    for (int i = 2; i < clause.size(); i++)
    {
        if (literal_status(clause_body[i]).is_true())
        {
            return AssignmentState::TRUE;
        }
    }

    return AssignmentState::UNKNOWN;
}

AssignmentState Engine::clause_status(const Clause& clause) const
{
    bool has_unknown = false;

    for (auto lit : clause)
    {
        AssignmentState status = literal_status(lit);

        if (status.is_true())
        {
            return AssignmentState::TRUE;
        }
        else if (status.is_unknown())
        {
            has_unknown = true;
        }
    }

    if (has_unknown)
    {
        return AssignmentState::UNKNOWN;
    }

    return AssignmentState::FALSE;
}

AssignmentState Engine::variable_status(Variable variable) const
{
    assert(variable > 0);
    assert(variable < _variable_states.size());
    return _variable_states[variable];
}

AssignmentState Engine::literal_status(Literal literal) const
{
    auto var_status = variable_status(literal.get_variable());
    return (literal.is_negated()) ? var_status.flipped() : var_status;
}

Engine::Status Engine::status() const
{
    bool has_unknowns = false;

    for (ClauseIdx idx = 0; idx < _clause_infos.size(); idx++)
    {
        const AssignmentState clause_state = (_next_unit_propagation == _trail.size())
                                                 ? clause_status(idx)
                                                 : clause_status(get_clause(idx));

        if (clause_state.is_false())
        {
            return Status::CONFLICT;
        }

        if (clause_state.is_unknown())
        {
            has_unknowns = true;
        }
    }

    if (has_unknowns)
    {
        return Status::UNKNOWN;
    }

    return Status::SOLVED;
}

bool Engine::is_trivially_unsat() const { return _trivial_unsat; }

Engine::ClauseIdx Engine::reason(Variable variable) const
{
    assert(variable > 0);
    assert(variable < _variable_infos.size());

    return _variable_infos[variable].reason;
}

std::optional<Engine::DecisionLevel>
Engine::lowest_unit_propagation_level(const Clause& clause) const
{
    assert(clause.size() > 0);

    bool has_unknown = false;
    DecisionLevel highest_seen = -1;
    DecisionLevel second_highest_seen = -1;

    // we can unit propagate a clause of size one already at the beginning
    if (clause.size() == 1)
    {
        return 0;
    }

    // we can assume that the clause has at least two literals, this is important
    for (Literal lit : clause)
    {
        Variable var = lit.get_variable();

        if (variable_status(var).is_unknown())
        {
            // if the clause has two unknown literals, we cannot unit propagate it
            // at any level
            if (has_unknown)
            {
                return std::nullopt;
            }

            has_unknown = true;
        }
        else
        {
            // if the variable is known, it always has a decision level
            DecisionLevel level = decision_level(var).value();

            if (highest_seen == -1)
            {
                highest_seen = level;
            }
            else if (second_highest_seen == -1)
            {
                second_highest_seen = std::min(highest_seen, level);
                highest_seen = std::max(highest_seen, level);
            }
            else if (level > highest_seen)
            {
                second_highest_seen = highest_seen;
                highest_seen = level;
            }
            else if (level > second_highest_seen)
            {
                second_highest_seen = level;
            }
        }
    }

    if (has_unknown)
    {
        // there is exactly one unknown literal; then we can unit propagate at
        // the level where the last literal was assigned
        return highest_seen;
    }

    // when we have no unknowns, we can unit propagate at the level where the
    // second-to-last literal was assigned
    // this must hold a valid value at this point, if there was only one literal,
    // we returned, if there were two unknowns, we return, if there was one unknown,
    // we returned, so at least two literals have to be assigned to get to this
    // point
    return second_highest_seen;
}

bool Engine::can_backtrack() const { return _decisions.size() > 0; }

Engine::DecisionLevel Engine::current_decision_level() const { return _decisions.size(); }

std::optional<Engine::DecisionLevel> Engine::decision_level(Variable variable) const
{
    assert(variable > 0);
    assert(variable < _variable_infos.size());
    auto level = _variable_infos[variable].decision_level;

    return (level == NO_LEVEL) ? std::nullopt : std::optional(level);
}

std::optional<Variable> Engine::last_decision() const
{
    if (_decisions.empty())
    {
        return std::nullopt;
    }

    return _trail[_decisions.back()].get_variable();
}

bool Engine::at_current_decision_level(Variable variable) const
{
    return decision_level(variable) == current_decision_level();
}

const std::vector<Variable>& Engine::unassigned_in_last_backtrack() const
{
    return _unassigned_in_last_backtrack;
}

AssignmentState Engine::last_assignment_state(Variable variable) const
{
    assert(variable > 0);
    assert(variable < _variable_infos.size());
    return _variable_infos[variable].last_assignment_state;
}

bool Engine::learn(Clause clause)
{
    assert(_clause_infos.size() <= INT_MAX);
    assert(clause.size() >= 1);
    assert(!clause_status(clause).is_false());

    if (clause.size() == 1)
    {
        Literal literal = clause[0];
        set_literal_to_true(literal);
        _variable_infos[literal.get_variable()].reason = UNIT_LITERAL;
        return false;
    }

    _clause_infos.emplace_back(std::move(clause));
    ClauseInfo& info = _clause_infos.back();
    ClauseIdx idx = _clause_infos.size() - 1;

    // we order the literals in the clause such that true literals are
    // first, unknown second, false last

    int true_idx = 0;
    int unknown_idx = 0;

    // in order to avoid checking where the clause begins every access
    auto clause_body = info.clause.begin();

    for (int i = 0; i < info.clause.size(); i++)
    {
        AssignmentState state = literal_status(clause_body[i]);

        if (state.is_true())
        {
            std::swap(clause_body[true_idx], clause_body[unknown_idx]);
            std::swap(clause_body[true_idx], clause_body[i]);

            unknown_idx++;
            true_idx++;
        }
        else if (state.is_unknown())
        {
            std::swap(clause_body[unknown_idx], clause_body[i]);
            unknown_idx++;
        }
    }

    if (true_idx == 0 && unknown_idx == 1)
    {
        // we can unit propagate the one unknown (now first) literal
        _variable_infos[clause_body[0].get_variable()].reason = idx;
        set_literal_to_true(clause_body[0]);
    }

    set_up_watch(idx, true);
    set_up_watch(idx, false);
    return true;
}

const std::vector<Engine::ClauseIdx>&
Engine::forget(const std::vector<ClauseIdx>& indices)
{
    const int REMOVED = -1;

    // invariant ... no value is true in this map when this function begins
    //
    // NOTE: if we ever want to use threads, this will have to become thread_local
    static std::vector<bool> remove_map;
    remove_map.resize(clause_count(), false);

    for (ClauseIdx idx : indices)
    {
        remove_map[idx] = true;
    }

    // indices ... of type ClauseIdx
    static std::vector<ClauseIdx> relocations;
    relocations.clear();
    relocations.reserve(clause_count());

    int new_size = 0;
    for (ClauseIdx idx = 0; idx < _clause_infos.size(); idx++)
    {
        if (_clause_infos[idx].clause.size() < 3 || idx < original_clause_count() ||
            !remove_map[idx])
        {
            // the move must happen into a local variable first, as loc = std::move[loc]
            // is undefined...
            ClauseInfo moved = std::move(_clause_infos[idx]);
            relocations.push_back(new_size);
            _clause_infos[new_size++] = std::move(moved);
        }
        else
        {
            relocations.push_back(REMOVED);
        }
        remove_map[idx] = false;
    }

    _clause_infos.erase(std::next(_clause_infos.begin(), new_size), _clause_infos.end());

    for (VariableInfo& variable_info : _variable_infos)
    {
        // These two blocks change the indices of clauses in the variables' watches
        int new_size = 0;
        for (int watch_i = 0; watch_i < variable_info.watched_negated.size(); watch_i++)
        {
            ClauseIdx watched_in = variable_info.watched_negated[watch_i];

            if (relocations[watched_in] == REMOVED)
            {
                continue;
            }

            variable_info.watched_negated[new_size++] = relocations[watched_in];
        }

        variable_info.watched_negated.shrink(new_size);

        new_size = 0;
        for (int watch_i = 0; watch_i < variable_info.watched_nonnegated.size();
             watch_i++)
        {
            ClauseIdx watched_in = variable_info.watched_nonnegated[watch_i];

            if (relocations[watched_in] == REMOVED)
            {
                continue;
            }

            variable_info.watched_nonnegated[new_size++] = relocations[watched_in];
        }

        variable_info.watched_nonnegated.shrink(new_size);

        if (variable_info.reason >= 0) // it corresponds to a clause index
        {
            variable_info.reason = relocations[variable_info.reason];
        }
    }

    return relocations;
}

void Engine::reset()
{
    backtrack_to(0);

    // this is needed specifically for DPLL with restarts, because if we
    // backtrack to the zeroth decision level, the flipped decisions
    //  will be included as well, which is not something we want
    clear_trail(_earliest_flipped_decision - 1);
    _earliest_flipped_decision = INT_MAX;
}

void Engine::backtrack_to(DecisionLevel level)
{
    assert(level >= 0);
    assert(level == -1 || level <= _trail.size());
    assert(level <= _highest_backtrack_level);

    _unassigned_in_last_backtrack.clear();

    if (level == _decisions.size())
    {
        return;
    }

    clear_trail(_decisions[level] - 1);

    // if we backtracked below the highest backtrack level, we need
    // to update the highest backtrack level
    _highest_backtrack_level = current_decision_level();
}

void Engine::flip_last_decision()
{
    assert(!_decisions.empty());
    assert(_decisions.back() < _trail.size());
    // we are practically backtracking one level
    assert(current_decision_level() - 1 <= _highest_backtrack_level);

    _earliest_flipped_decision = std::min(_earliest_flipped_decision, _decisions.back());

    const TrailIdx last_decision_idx = _decisions.back();
    _decisions.pop_back();

    clear_trail(last_decision_idx);

    Literal last_decision = _trail.back();
    unassign_last();

    set_literal_to_true(last_decision.negated());

    // if we backtracked below the highest backtrack level, we need
    // to update the highest backtrack level
    _highest_backtrack_level = current_decision_level();
}

void Engine::decide(Variable variable, VariablePhase phase)
{
    assert(variable > 0);
    assert(variable < _variable_infos.size());
    assert(_variable_states[variable].is_unknown());

    // the order is important here!
    _decisions.push_back(_trail.size());
    assign(variable, phase);
}

std::optional<Engine::ClauseIdx> Engine::unit_propagate()
{
    while (_next_unit_propagation < _trail.size())
    {
        // shortcuts
        const Literal literal = _trail[_next_unit_propagation];
        const Variable variable = literal.get_variable();
        VariableInfo& info = _variable_infos[variable];
        const AssignmentState variable_state = _variable_states[variable];

        auto& watched =
            (variable_state.is_true()) ? info.watched_negated : info.watched_nonnegated;

        std::optional<ClauseIdx> conflict_clause = std::nullopt;

        int new_size = 0;
        for (int watch_i = 0; watch_i < watched.size(); watch_i++)
        {
            ClauseIdx& idx = watched[watch_i];

            // do not bother with reassigning the other watches when
            // we already know there is a conflict
            if (conflict_clause.has_value())
            {
                watched[new_size++] = idx;
                continue;
            }

            Clause& clause = _clause_infos[idx].clause;
            // in order to avoid checking where it is stored every access
            auto clause_body = clause.begin();

            assert(clause.size() > 1);

            // do not waste time reassigning watches when the clause is true
            if (literal_status(clause_body[0]).is_true() ||
                literal_status(clause_body[1]).is_true())
            {
                watched[new_size++] = idx;
                continue;
            }

            int to_reassign = (clause_body[0].get_variable() == variable) ? 0 : 1;
            int other_watch = (to_reassign == 0) ? 1 : 0;

            assert(literal_status(clause_body[to_reassign]).is_false());

            bool reassigned = false;
            for (int i = 2; i < clause.size(); i++)
            {
                if (!literal_status(clause_body[i]).is_false())
                {
                    std::swap(clause_body[i], clause_body[to_reassign]);
                    set_up_watch(idx, to_reassign == 0);
                    reassigned = true;
                    break;
                }
            }

            if (!reassigned)
            {
                watched[new_size++] = idx;

                if (literal_status(clause_body[other_watch]).is_false())
                {
                    // we cannot return here, some watches may already be reassigned,
                    // we have to finish the reassigning
                    conflict_clause = idx;
                }
                else // the other watch is unknown, we will unit propagate it
                {
                    set_literal_to_true(clause_body[other_watch]);
                    _variable_infos[clause_body[other_watch].get_variable()].reason = idx;
                }
            }
        }

        watched.shrink(new_size);
        _next_unit_propagation++;

        if (conflict_clause.has_value())
        {
            return conflict_clause;
        }
    }

    _highest_backtrack_level = current_decision_level();
    return std::nullopt;
}

void Engine::assign(Variable variable, VariablePhase phase)
{
    assert(variable < _variable_infos.size());
    assert(variable_status(variable).is_unknown());

    VariableInfo& info = _variable_infos[variable];

    _trail.push_back(Literal::from_variable(variable, phase));
    _variable_states[variable].assign(phase);
    info.last_assignment_state.assign(phase);
    info.decision_level = _decisions.size();

    // we do not change watches here anymore, that happens in unit propagation
}

void Engine::set_literal_to_true(Literal lit) { assign(lit.get_variable(), lit.phase()); }

void Engine::unassign_last()
{
    assert(!_trail.empty());
    Variable to_unassign = _trail.back().get_variable();
    _trail.pop_back();

    assert(to_unassign > 0);
    assert(to_unassign < _variable_infos.size());

    VariableInfo& info = _variable_infos[to_unassign];

    _variable_states[to_unassign].unassign();
    info.reason = -1;
    info.decision_level = -1;
    _next_unit_propagation = std::min(_next_unit_propagation, (int)_trail.size());

    _unassigned_in_last_backtrack.push_back(to_unassign);

    // the removed assignment was a decision
    if (!_decisions.empty() && _decisions.back() == _trail.size())
    {
        _decisions.pop_back();
    }
}

void Engine::clear_trail(TrailIdx from)
{
    assert(-1 <= from);

    // if we have more than INT_MAX variables, we are not solving it anyways
    assert(_trail.size() < INT_MAX);

    // the expression on the right returns a size_t, which is unsigned;
    // if the two things were compared directly and from was -1, the behaviour
    // would be surprising and incorrect
    while (from < (int)_trail.size() - 1)
    {
        unassign_last();
    }
}

void Engine::set_up_watch(ClauseIdx clause_idx, bool first)
{
    const Literal& lit = get_clause(clause_idx)[first ? 0 : 1];

    if (lit.is_negated())
    {
        _variable_infos[lit.get_variable()].watched_negated.push_back(clause_idx);
    }
    else
    {
        _variable_infos[lit.get_variable()].watched_nonnegated.push_back(clause_idx);
    }
}
