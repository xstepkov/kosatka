#include <fstream>
#include <iostream>
#include <optional>
#include <string_view>
#include <charconv>

#include "formula.hpp"
#include "parser.hpp"

enum class LineType
{
    COMMENT,
    PROBLEM,
    PERCENT,
    OTHER,
};

static LineType get_line_type(std::string_view line)
{
    if (line.size() > 0)
    {
        switch (line.front())
        {
        case 'c':
            return LineType::COMMENT;
        case 'p':
            return LineType::PROBLEM;
        case '%':
            return LineType::PERCENT;
        default:
            return LineType::OTHER;
        }
    }

    return LineType::OTHER;
}

void skip_whitespace(std::string_view& line)
{
    int first_nonspace = line.find_first_not_of(" \n\t");
    if (first_nonspace == -1)
    {
        first_nonspace = line.size();
    }

    line.remove_prefix(first_nonspace);
}

std::tuple<int, std::string_view> parse_integer(std::string_view line)
{
    // parse first integer in line
    // expects that the line starts with the integer (does not ignore whitespace)

    int parsed_num;
    auto res = std::from_chars(line.data(), line.data() + line.size(), parsed_num);
    
    if (res.ptr == line.data())
    {
        throw std::invalid_argument("Error when parsing integers");
    }

    int read_count = res.ptr - line.data();
    line.remove_prefix(read_count);
    return {parsed_num, line};
}

static bool parse_integer_string(std::string_view line, std::vector<int>& result)
{
    // read a sequence of integers divided by whitespace until the first 0
    // return true if encountered 0

    bool encountered_zero = false;
    std::from_chars_result res;
    skip_whitespace(line);

    while (!line.empty())
    {
        auto [parsed_num, unparsed] = parse_integer(line);
        line = unparsed;
        skip_whitespace(line);

        if (parsed_num != 0)
        {
            result.push_back(parsed_num);
        }
        else
        {
            encountered_zero = true;
            break;
        }
    }

    return encountered_zero;
}

static void parse_problem_line(std::string_view line, int& num_variables, int& num_clauses)
{
    std::string prefix = "p cnf ";
    if (line.compare(0, prefix.size(), prefix) != 0)
    {
        throw std::invalid_argument("Invalid problem line");
    }

    std::string_view suffix = line.substr(prefix.size(), line.size() - prefix.size());
    std::vector<int> parsed_numbers;
    parse_integer_string(suffix, parsed_numbers);

    if (parsed_numbers.size() != 2)
    {
        throw std::invalid_argument("Invalid problem line");
    }

    num_variables = parsed_numbers.at(0);
    num_clauses = parsed_numbers.at(1);
}

static bool parse_clause_line(std::string_view line, std::vector<int>& store)
{
    return parse_integer_string(line, store);
}

static bool validate_variables(Formula& formula, int num_variables)
{
    for (auto& clause : formula.body)
    {
        for (auto& literal : clause)
        {
            if (literal.get_variable() > num_variables)
            {
                return false;
            }
        }
    }
    return true;
}

static Formula read_file(std::ifstream& file)
{
    std::string line;
    std::vector<int> clause_body;

    bool comment_section = true;
    bool percent_encountered = false;

    int num_variables = -1;
    int num_clauses = -1;

    std::vector<Clause> clauses;

    while (getline(file, line) && !percent_encountered)
    {
        LineType line_type = get_line_type(line);

        switch (line_type)
        {
        case LineType::COMMENT:
            if (!comment_section)
            {
                throw std::invalid_argument("Unexpected comment");
            }
            break;

        case LineType::PROBLEM:
            comment_section = false;
            parse_problem_line(line, num_variables, num_clauses);

            clauses.reserve(num_clauses);
            break;

        case LineType::PERCENT:
            // non-mandatory end of file, stop reading
            percent_encountered = true;
            break;

        case LineType::OTHER:
            if (num_variables == -1 || num_clauses == -1)
            {
                throw std::invalid_argument("Expecting problem line");
            }

            bool clause_finished = parse_clause_line(line, clause_body);
            if (clause_finished)
            {
                clauses.push_back(Clause::from_numbers(clause_body));
                clause_body.clear();
            }
            break;
        }
    }

    if (clauses.size() != num_clauses)
    {
        throw std::invalid_argument(
            "Specified and actual number of clauses do not match");
    }

    Formula formula = Formula(std::move(clauses), num_variables, num_clauses);

    if (!validate_variables(formula, num_variables))
    {
        throw std::invalid_argument(
            "Formula contains a variable out of the specified scope");
    }

    return formula;
}

std::optional<Formula> parse_formula(const std::string& path)
{
    std::ifstream file;
    file.open(path.data());

    if (file.fail())
    {
        std::cerr << "c Error occured when reading the source file\n";
        return std::nullopt;
    }

    try
    {
        Formula formula = read_file(file);
        return formula;
    }
    catch (const std::exception& e)
    {
        std::cerr << "c " << e.what() << '\n';
        return std::nullopt;
    }
}
