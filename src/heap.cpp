#include "heap.hpp"
#include <numeric>

Heap::Heap(int elements_count)
    : _heap(elements_count, 0),
      _activities(elements_count + 1, 1.0),
      _heap_end{static_cast<int>(_heap.size())},
      _positions(elements_count + 1, 0)
{
    // each variable is initially stored at its identifier as index
    std::iota(_heap.begin(), _heap.end(), 1);
    std::iota(_positions.begin(), _positions.end(), -1);
}

Variable Heap::extract_max()
{
    assert(!empty());

    Variable max = _heap[0];
    swap(_heap[0], _heap[_heap_end - 1]);
    --_heap_end;
    heapify_down(0);

    return max;
}

void Heap::increase_activity(Variable var, double new_act)
{
    _activities[var] = new_act;

    heapify_up(_positions[var]);
}

void Heap::insert(Variable var)
{
    if (_positions[var] < _heap_end)
        return; // variable already in heap

    swap(var, _heap[_heap_end]);
    ++_heap_end;
    heapify_up(_positions[var]);
}

void Heap::decrease_all_activities(double decrease_by)
{
    for (auto& act : _activities)
    {
        act /= decrease_by;
    }
}

// move an element at index i down in the heap to renew the heap property
void Heap::heapify_down(int i)
{
    int largest = i;
    do
    {
        if (largest != i)
        {
            swap(_heap[i], _heap[largest]);
        }
        i = largest;

        if (int l = left(i); l < _heap_end && gt(_heap[l], _heap[largest]))
        {
            largest = l;
        }
        if (int r = right(i); r < _heap_end && gt(_heap[r], _heap[largest]))
        {
            largest = r;
        }
    } while (largest != i);
}

// move an element at index i up in the heap to renew the heap property
void Heap::heapify_up(int i)
{
    if (i >= _heap_end)
        return;

    while (i > 0 && gt(_heap[i], _heap[parent(i)]))
    {
        swap(_heap[i], _heap[parent(i)]);
        i = parent(i);
    }
}
