#include <cassert>
#include <restart_manager.hpp>

std::unique_ptr<RestartManager> RestartManager::choose(RestartManagerType type,
                                                       int restart_constant)
{
    switch (type)
    {
    case RestartManagerType::NO:
        return std::make_unique<NoRestartManager>();
    case RestartManagerType::CONSTANT:
        return std::make_unique<ConstantRestartManager>(restart_constant);
    case RestartManagerType::LUBY:
        return std::make_unique<LubyRestartManager>(restart_constant);
    default:
        assert(false);
    }
}

bool NoRestartManager::should_restart() { return false; }

bool ConstantRestartManager::should_restart()
{
    if (_conflicts_since_last >= _restart_after)
    {
        _conflicts_since_last = 0;
        return true;
    }

    return false;
}

bool LubyRestartManager::should_restart()
{
    if (_conflicts_since_last >= _restart_after)
    {
        _conflicts_since_last = 0;
        _restart_after = next();
        return true;
    }

    return false;
}

int LubySequenceGenerator::next()
{
    int to_return = _current;
    if (_current == _next_reach)
    {
        _sum_of_last = 0;
        _current = 1;
        _next_reach *= 2;
    }
    else
    {
        _sum_of_last += _current;
        _current = _sum_of_last;
    }
    return to_return;
}
