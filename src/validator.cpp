#include "validator.hpp"

#include <unordered_set>

Validator::Validator(const Formula& formula)
    : formula(formula)
{
}

bool Validator::validate(const std::vector<Literal>& model) const
{
    std::unordered_set<Literal, Literal::Hash> mset{model.begin(), model.end()};

    for (const auto& clause : formula.body)
    {
        bool satisfied = false;

        for (const auto& literal : clause)
        {
            if (mset.find(literal) != mset.end())
            {
                satisfied = true;
                break;
            }
        }

        if (!satisfied)
        {
            return false;
        }
    }

    return true;
}
