#include "threeval.hpp"

const AssignmentState AssignmentState::TRUE =
    AssignmentState(AssignmentState::State::TRUE);

const AssignmentState AssignmentState::FALSE =
    AssignmentState(AssignmentState::State::FALSE);

const AssignmentState AssignmentState::UNKNOWN =
    AssignmentState(AssignmentState::State::UNKNOWN);

AssignmentState::State AssignmentState::from_bool(bool b)
{
    return (b) ? State::TRUE : State::FALSE;
}

AssignmentState::State AssignmentState::from_phase(VariablePhase phase)
{
    switch (phase)
    {
    case VariablePhase::POSITIVE:
        return State::TRUE;
    case VariablePhase::NEGATIVE:
        return State::FALSE;
    default:
        assert(false);
    }
}

AssignmentState::AssignmentState()
    : state(State::UNKNOWN)
{
}

bool AssignmentState::is_decided() const { return state != State::UNKNOWN; }

bool AssignmentState::is_unknown() const { return state == State::UNKNOWN; }

bool AssignmentState::is_true() const { return state == State::TRUE; }

bool AssignmentState::is_false() const { return state == State::FALSE; }

std::optional<VariablePhase> AssignmentState::to_phase() const
{
    switch (state)
    {
    case State::TRUE:
        return VariablePhase::POSITIVE;
    case State::FALSE:
        return VariablePhase::NEGATIVE;
    case State::UNKNOWN:
        return std::nullopt;
    default:
        assert(false);
    }
}

AssignmentState AssignmentState::flipped() const
{
    switch (state)
    {
    case State::UNKNOWN:
        return AssignmentState::UNKNOWN;
    case State::TRUE:
        return AssignmentState::FALSE;
    case State::FALSE:
        return AssignmentState::TRUE;
    default:
        assert(false);
    }
}

void AssignmentState::flip()
{
    switch (state)
    {
    case State::UNKNOWN:
        return;
    case State::TRUE:
        state = State::FALSE;
        break;
    case State::FALSE:
        state = State::TRUE;
        break;
    default:
        assert(false);
    }
}

void AssignmentState::assign(bool value) { state = from_bool(value); }

void AssignmentState::assign(VariablePhase phase) { state = from_phase(phase); }

void AssignmentState::unassign() { state = State::UNKNOWN; }
