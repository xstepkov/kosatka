#include "phase_picker.hpp"

std::unique_ptr<PhasePicker> PhasePicker::choose(PhasePickerType type)
{
    switch (type)
    {
    case PhasePickerType::BASIC:
        return std::make_unique<BasicPhasePicker>();
    case PhasePickerType::SAVING:
        return std::make_unique<SavingPhasePicker>();
    default:
        assert(false);
    }
}

VariablePhase BasicPhasePicker::pick(Variable var, const Engine&)
{
    return VariablePhase::NEGATIVE;
}

VariablePhase SavingPhasePicker::pick(Variable var, const Engine& engine)
{
    auto last_assignment = engine.last_assignment_state(var).to_phase();
    return last_assignment.value_or(SavingPhasePicker::_default);
}
