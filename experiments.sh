#!/bin/bash

# ------------- parameters -------------------------
executable="./build/tools/kosatka"
benchmarks_list="./experiments/benchmarks.txt"
csv_file="./experiments/optimized_CDCL_TWL_EVSIDScorrect_REST-LUBY2_PHASE.csv"
csv_header="name,system_time,user_time,memory"
args=""

# ------------- run experiments --------------------
if [ -f "$csv_file" ]; then
    echo "specified csv file already exists!"
    exit 1
fi

echo $csv_header > $csv_file

cat $benchmarks_list | while read file
do
    if [ -f "$file" ]; then
        current_time=$(date +"%H:%M:%S")
        echo "$current_time $file"

        timeout "2m" $executable $args -q -d $csv_file $file 1> "/dev/null" || echo "$file" >> $csv_file
    fi
done
