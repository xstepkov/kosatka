# koSATka

**koSATka** is a simple SAT solver implemented in C++.

## Compilation

### Prerequisities

**koSATka** is developed primarily for Linux. It may work on MacOS and
Windows, but we give no guarantees. If you want to download and compile
Kosatka, you need the following software:

* git
* clang (version 5 or higher)
* cmake (version 3.10 or higher)
* make

All of these should be available on most Linux distributions either directly,
of they can be downloaded via a package manager (apt, dnf, pacman...).

### Download & Build

You can download the source code of **koSATka** by cloning it from Gitlab:

```
git clone https://gitlab.fi.muni.cz/xstepkov/kosatka.git
```

To build **koSATka**, execute the following commands:

```
cmake -S kosatka -B kosatka/build
cd kosatka/build
make
cd ../..
```

If the build process was successful, the `kosatka` executable will be
available in the `./kosatka/build/tools` directory.

## Capabilities

**koSATka** is a simple SAT solver that has the follwing features:

* It can solve SAT formulas and generate models for satisfactory formulas
* It can minimize generated models
* It can give basic diagnostic information about its resource consumption

In the future, we may to extend **koSATka** to support:

* Proof generation for unsatisfactory formulas
* Unsat core generation
* Solving under assumptions

Internally, **koSATka** uses the following algorithms and heuristics:

* The core algorithm is CDCL, but DPLL is also available, albeit with
  fewer heuristics
* **koSATka** uses two-watched-literals scheme for unit propagation
* The default variable heuristic is EVSIDS; it can be turned off
* The default phase selection strategy is phase saving; it can be turned off
* There is a simple algorithm for learnt clause minimization
* **koSATka** can use VSIDS for forgetting learnt clauses
* Restarts happen periodically using the Luby sequence, but constant restarts
  are also implemented

## Development

**koSATka** has been optimized to run fast. Because of that, there are several
things that need to be considered for future extensions.

* **koSATka** uses `static` lifetime for some data structures to avoid
  allocations. This means that for some classes, it is assumed that only
  one instance exists. It also assumes that only one thread exists.

## Usage

**koSATka** is a SAT solver that receives formulas in CNF in the DIMACS
format. The output is given as described by the SAT competition. For
unsatisfiable formulas, no proof is generated as of now.

To solve the problem in `formula.cnf` and print the result to the
standard output, execute the following:

```
./kosatka/build/tools/kosatka formula.cnf
```

* Use `-q` or `--quiet` if you do not wish to get a model of a satisfiable
formula.

* Use `-h` or `--help` to generate a help text.

* Use `-d` or `--diagnostics` followed by a file name if you want to
save diagnostic data into an csv file. First goes the name of the
benchmark, followed by the system time usage (in microseconds),
then by user time usage (in microseconds), and finally by the
maximum number of memory required (in kilobytes).

* Use `--algorithm` followed by a name of an algorithm to choose the
the SAT algorithm. Default is `cdcl`, but you can also use `dpll`.
`dpll` has fewer options, as far as the following heuristics are
concerned. Restarts, minimization, vsids and forgetting are not
possible.

* Use `--no-minimize` to turn off minimization of learnt clauses.

* Use `--minimize-model` to minimize the returned model if the formula
was satisfiable.

* Use `--variable-picker` followed by a name of a variable picker to
choose a variable decision heuristic. Default is `evsids`. `basic`
variable picker selects the smallest unassigned variable.

* Use `--phase-picker` to choose a heuristic for choosing the phase
of a variable. Default is `saving`, but `basic` is also available,
which sets all variables to `false` by default.

* Use `--restart-manager` to choose the restart strategy. Default is
`luby` (which uses the Luby sequence), but `constant` (restarts
always happen after a specific number of conflicts) and `no`
(restarts are turned off) are also available.

* Use `--restart-constant` to choose the starting number of conflicts
before a restart happends. The default value is 80.

* Use `--no-forgetting` to turn off clause forgetting. Forgetting
is implicitely turned off when restarts are turned off.

* Use `--forgetting-constant` followed by an integer greater than one
to set the forgetting constant to said number. Determines how many
conflicts have to happen before a forgetting triggers. Recommended
to be in the thousands. The default value is 1000.

* Use `--forget-precentage` to select how many clauses are forgotten
during a forgetting. The default value is `0.66`, i.e. about two thirds
of all clauses are forgotten with each forget. Keep in mind that
some combinations of other parameters and a high forget percentage
can make **koSATka** incomplete.

## Authors

* Adéla Štěpková
* Jindřich Sedláček
