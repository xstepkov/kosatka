#include <charconv>
#include <fstream>
#include <iostream>
#include <optional>
#include <string>

#include "formula.hpp"
#include "parser.hpp"
#include "restart_manager.hpp"
#include "solver.hpp"
#include "utilities/resource.hpp"
#include "utilities/result.hpp"

class CommandLineOptions;
class CommandLineOptionsReader;

class CommandLineOptions
{
    bool print_help = false;
    bool quiet = false;

    Solver::SolverAlgorithm solver_algorithm = Solver::SolverAlgorithm::CDCL;

    bool no_minimize = false;
    bool minimize_model = false;

    RestartManagerType restart_manager_type = RestartManagerType::LUBY;
    int restart_constant = 80;

    VariablePickerType variable_picker_type = VariablePickerType::EVSIDS;

    PhasePickerType phase_picker_type = PhasePickerType::SAVING;

    bool no_forgetting = false;
    int forgetting_constant = 1000;
    double forget_percentage = 0.66;

    std::string input_file = "";
    std::string diagnostics_file = "";

public:
    void make_consistent()
    {
        if (solver_algorithm == Solver::SolverAlgorithm::DPLL)
        {
            if (restart_manager_type != RestartManagerType::NO)
            {
                std::cerr << "c Warning: restarts not supported for DPLL"
                             ", turning them off...\n";
                restart_manager_type = RestartManagerType::NO;
            }

            if (variable_picker_type == VariablePickerType::EVSIDS)
            {
                std::cerr << "c Warning: EVSIDS not supported for DPLL"
                             ", turning it off...\n";
                variable_picker_type = VariablePickerType::BASIC;
            }

            if (!no_forgetting)
            {
                std::cerr << "c Warning: Forgetting has no effect on DPLL\n";
            }

            if (no_minimize)
            {
                std::cerr << "c Warning: Minimization of clauses has no effect on DPLL\n";
            }
        }

        if (!no_forgetting && forgetting_constant * (1 - forget_percentage) <= 1 &&
            restart_manager_type == RestartManagerType::CONSTANT)
        {
            std::cerr << "c Warning: The forgetting constant is too low and the restart"
                         " manager type is constant, koSATka may be incomplete\n";
        }
    }

    bool asked_to_print_help() const { return print_help; }
    bool asked_to_be_quiet() const { return quiet; }

    const std::string& get_input_file() const { return input_file; }
    const std::string& get_diagnostics_file() const { return diagnostics_file; }
    Solver::SolverAlgorithm get_solver_algorithm() const { return solver_algorithm; }

    Solver::Options get_solver_options() const
    {
        return {solver_algorithm,
                !no_minimize,
                variable_picker_type,
                phase_picker_type,
                restart_manager_type,
                restart_constant,
                (no_forgetting) ? ForgettingManagerType::NO
                                : ForgettingManagerType::VSIDS,
                forgetting_constant,
                forget_percentage,
                minimize_model};
    }

    friend CommandLineOptionsReader;
};

class CommandLineOptionsReader
{
    int argc;
    char** argv;

    CommandLineOptions options;

    // we decrement argc and increment argv, because we do not really care
    // about the first argument
    CommandLineOptionsReader(int argc, char** argv)
        : argc(--argc),
          argv(++argv)
    {
    }

    void forward()
    {
        --argc;
        ++argv;
    }

    void backward()
    {
        ++argc;
        --argv;
    }

    // If one of the specified names is read, the answer is set to true
    bool read_boolean_option(bool& answer, std::initializer_list<std::string> names)
    {
        if (argc <= 0)
        {
            return false;
        }
        for (const auto& name : names)
        {
            if (std::string(*argv) == name)
            {
                answer = true;
                forward();
                return true;
            }
        }

        return false;
    }

    enum class ReadResult
    {
        NOT_MATCHED,
        NOT_VALID,
        VALID
    };

    // If one of the names is read, transform the following option into T and store
    // it in answer
    template <typename T, typename F>
    ReadResult read_argument_option(T& answer, std::initializer_list<std::string> names,
                                    F sanitizer)
    {
        if (argc <= 1)
        {
            return ReadResult::NOT_MATCHED;
        }

        for (const auto& name : names)
        {
            // TODO: allow also --name=value
            if (std::string(*argv) == name)
            {
                forward();
                std::optional<T> sanitizer_result = sanitizer(*argv);

                if (sanitizer_result.has_value())
                {
                    answer = sanitizer_result.value();
                    forward();
                    return ReadResult::VALID;
                }

                return ReadResult::NOT_VALID;
            }
        }

        return ReadResult::NOT_MATCHED;
    }

    // If one of the names is read, save the following option into answer
    bool read_string_option(std::string& answer, std::initializer_list<std::string> names)
    {
        if (argc <= 1)
        {
            return false;
        }

        for (const auto& name : names)
        {
            if (std::string(*argv) == name)
            {
                forward();
                answer = *argv;
                forward();
                return true;
            }
        }

        return false;
    }

    EmptyResult run()
    {
        while (argc > 0)
        {
            if (read_boolean_option(options.print_help, {"-h", "--help"}))
            {
                // we do not do anything else if the user wants help
                return EmptyResult::Ok({});
            }

            // the order is important here - this has to be the second thing
            // here, help has to be the first, for the others it should not
            // matter
            if (argc == 1)
            {
                options.input_file = *argv;
                return EmptyResult::Ok({});
            }

            if (read_boolean_option(options.quiet, {"-q", "--quiet"}))
            {
                continue;
            }

            if (read_boolean_option(options.no_minimize, {"--no-minimize"}))
            {
                continue;
            }

            if (read_boolean_option(options.minimize_model, {"--minimize-model"}))
            {
                continue;
            }

            if (read_boolean_option(options.no_forgetting, {"--no-forgetting"}))
            {
                continue;
            }

            constexpr auto positive_integer_sanitizer =
                [](const std::string& s) -> std::optional<int>
            {
                int num;

                auto [ptr, ec] = std::from_chars(s.data(), s.data() + s.size(), num);

                if (ec == std::errc() && num > 0)
                {
                    return num;
                }

                return std::nullopt;
            };

            switch (read_argument_option(options.forgetting_constant,
                                         {"--forgetting-constant"},
                                         positive_integer_sanitizer))
            {
            case ReadResult::NOT_VALID:
                return EmptyResult::Error(
                    "Forgetting constant must be a positive integer");
            case ReadResult::VALID:
                continue;
            case ReadResult::NOT_MATCHED:
                break;
            }

            constexpr auto percentage_sanitizer =
                [](const std::string& s) -> std::optional<double>
            {
                double num;

                auto [ptr, ec] = std::from_chars(s.data(), s.data() + s.size(), num);

                if (ec == std::errc() && num > 0.0 && num < 1.0)
                {
                    return num;
                }

                return std::nullopt;
            };

            switch (read_argument_option(options.forget_percentage,
                                         {"--forget-percentage"}, percentage_sanitizer))
            {
            case ReadResult::NOT_VALID:
                return EmptyResult::Error(
                    "The forget percentage must be a decimal number between 0 and 1");
            case ReadResult::VALID:
                continue;
            case ReadResult::NOT_MATCHED:
                break;
            }

            if (read_string_option(options.diagnostics_file, {"-d", "--diagnostics"}))
            {
                continue;
            }

            constexpr auto algorithm_sanitizer =
                [](const std::string& s) -> std::optional<Solver::SolverAlgorithm>
            {
                if (s == "dpll")
                {
                    return Solver::SolverAlgorithm::DPLL;
                }

                if (s == "cdcl")
                {
                    return Solver::SolverAlgorithm::CDCL;
                }

                return std::nullopt;
            };

            switch (read_argument_option(options.solver_algorithm, {"--algorithm"},
                                         algorithm_sanitizer))
            {
            case ReadResult::NOT_VALID:
                return EmptyResult::Error("Algorithm can only by \"dpll\" or \"cdcl\"");
            case ReadResult::VALID:
                continue;
            case ReadResult::NOT_MATCHED:
                break;
            }

            constexpr auto variable_picker_sanitizer =
                [](const std::string& s) -> std::optional<VariablePickerType>
            {
                if (s == "basic")
                {
                    return VariablePickerType::BASIC;
                }

                // also check upper case??? maybe automatically convert to lower case???
                if (s == "evsids")
                {
                    return VariablePickerType::EVSIDS;
                }

                return std::nullopt;
            };

            switch (read_argument_option(options.variable_picker_type,
                                         {"--variable-picker"},
                                         variable_picker_sanitizer))
            {
            case ReadResult::NOT_VALID:
                return EmptyResult::Error(
                    "Variable picker type can only by \"basic\" or \"evsids\"");
            case ReadResult::VALID:
                continue;
            case ReadResult::NOT_MATCHED:
                break;
            }

            constexpr auto phase_picker_sanitizer =
                [](const std::string& s) -> std::optional<PhasePickerType>
            {
                if (s == "basic")
                {
                    return PhasePickerType::BASIC;
                }

                // also check upper case??? maybe automatically convert to lower case???
                if (s == "saving")
                {
                    return PhasePickerType::SAVING;
                }

                return std::nullopt;
            };

            switch (read_argument_option(options.phase_picker_type, {"--phase-picker"},
                                         phase_picker_sanitizer))
            {
            case ReadResult::NOT_VALID:
                return EmptyResult::Error(
                    "Phase picker type can only by \"basic\" or \"saving\"");
            case ReadResult::VALID:
                continue;
            case ReadResult::NOT_MATCHED:
                break;
            default:
                assert(false);
            }

            constexpr auto restart_manager_sanitizer =
                [](const std::string& s) -> std::optional<RestartManagerType>
            {
                if (s == "no")
                {
                    return RestartManagerType::NO;
                }

                if (s == "constant")
                {
                    return RestartManagerType::CONSTANT;
                }

                if (s == "luby")
                {
                    return RestartManagerType::LUBY;
                }

                return std::nullopt;
            };

            switch (read_argument_option(options.restart_manager_type,
                                         {"--restart-manager"},
                                         restart_manager_sanitizer))
            {
            case ReadResult::NOT_VALID:
                return EmptyResult::Error(
                    "Restart manager can only by \"no\", constant or \"luby\"");
            case ReadResult::VALID:
                continue;
            case ReadResult::NOT_MATCHED:
                break;
            }

            switch (read_argument_option(options.restart_constant, {"--restart-constant"},
                                         positive_integer_sanitizer))
            {
            case ReadResult::NOT_VALID:
                return EmptyResult::Error("Restart constant must be an integer");
            case ReadResult::VALID:
                continue;
            case ReadResult::NOT_MATCHED:
                break;
            }

            // adding other command line options should be relatively easy from now on

            return EmptyResult::Error("Unknown option: " + std::string(*argv));
        }

        return EmptyResult::Error("No input file given!");
    }

public:
    static Result<CommandLineOptions> read(int argc, char** argv)
    {
        CommandLineOptionsReader reader(argc, argv);

        // the line below works, but is probably not very readable :-)
        // return reader.run().fmap<CommandLineOptions>([&reader](auto){ return
        // reader.options; });

        auto res = reader.run();

        if (res.is_error())
        {
            return res.forward_error<CommandLineOptions>();
        }

        return Result<CommandLineOptions>::Ok(reader.options);
    }
};

void print_help()
{
    std::cout
        << "Usage: kosatka [options] file\n"
           "Options:\n"

           "\t--help, -h\t\t\t\tShow this helpful message"
           " and do nothing else\n"

           "\t--quiet, -q\t\t\t\tDo not show models for"
           " satisfiable formulas\n"

           "\t--diagnostics <file>, -d <file>\t\t"
           "Append diagnostic information to a csv file\n"

           "\t--algorithm <alg>\t\t\t"
           "Choose algorithm, either cdcl (default) or dpll\n"

           "\t--minimize-model\t\t\t"
           "If the formula is SAT, minimize the generated model\n"

           "\t--no-minimize\t\t\t\t"
           "Do not minimize learnt clauses\n"

           "\t--no-forgetting\t\t\t\t"
           "Do not forget learnt clauses\n"

           "\t--forgetting-constant <num>\t\t"
           "Choose a minimum number of conflicts before forgetting triggers; "
           "must be a positive integer (default = 1000)\n"

           "\t--forget-percentage <perc>\t\t"
           "Choose what percentage of learnt clauses is forgotten when forgetting"
           " triggers (default = 0.66)\n"

           "\t--variable-picker <picker>\t\t"
           "Choose a variable picker, either basic or evsids (default)\n"

           "\t--phase-picker <picker>\t\t\t"
           "Choose a phase picker, either basic or saving (default)\n"

           "\t--restart-manager <manager>\t\t"
           "Choose a restart manager, either no, constant or luby (default)\n"

           "\t--restart-constant <num>\t\t"
           "Choose a restart constant, must be a positive integer (default = 80)\n";
}

void save_diagnostic_data(const CommandLineOptions& opts)
{
    std::ofstream f(opts.get_diagnostics_file(), std::ios::app);

    if (!f.is_open())
    {
        std::cerr << "Error: could not open file to write diagnostic data!";
        return;
    }

    auto time_usage = TimeUsage::now();
    auto memory_usage = SpaceUsage::maximum();

    // we assume that the diagnostics file is on a new line
    // format: name, system time, user time, memory
    // time is in microseconds
    // memory usage in kilobytes

    f << opts.get_input_file() << ',';
    f << time_usage.system_time<TimeUsage::Unit::USEC>() << ',';
    f << time_usage.user_time<TimeUsage::Unit::USEC>() << ',';
    f << memory_usage << std::endl;
}

int main(int argc, char* argv[])
{
    auto optionsResult = CommandLineOptionsReader::read(argc, argv);

    if (optionsResult.is_error())
    {
        std::cerr << "c Error: " << optionsResult.get_error_message()
                  << "\nc More information with \"kosatka -h\"\n";
        return 1;
    }

    auto options = optionsResult.get_value();

    options.make_consistent();

    if (options.asked_to_print_help())
    {
        print_help();
        return 0;
    }

    std::optional<Formula> formula = parse_formula(options.get_input_file());

    if (!formula.has_value())
    {
        std::cerr << "c Error: could not parse the formula in "
                  << options.get_input_file() << '\n';
        return 1;
    }

    auto algorithm = options.get_solver_algorithm();

    Solver solver(std::move(formula.value()), options.get_solver_options());

    SolverResult res = solver.run();
    std::vector<Literal> model;

    switch (res)
    {
    case SolverResult::SAT:
        std::cout << "s SATISFIABLE\n";

        if (!options.asked_to_be_quiet())
        {
            model = solver.get_model();

            std::cout << "v ";

            for (auto term : model)
            {
                std::cout << term << " ";
            }

            std::cout << "0\n";
        }

        break;

    case SolverResult::UNSAT:
        std::cout << "s UNSATISFIABLE\n";
        break;

    case SolverResult::UNKNOWN:
        std::cout << "s UNKNOWN\n";
        break;

    default:
        assert(false);
    }

    if (options.get_diagnostics_file() != "")
    {
        save_diagnostic_data(options);
    }

    return 0;
}
